#include "include/pstrings.h"
#include "include/server_variables.h"
#include "include/core_cpp_implement.h"
#include "include/data_enc.h"
#include "include/files.h"
#include "include/socket_implement.h"
#include "include/ClientDatabaseHandler.h"
#include "include/commandProcessor.h"
#include "include/wx_implement.h"
#include "include/wx_class.h"
#include "include/wx_class_implement.h"





//this function of class server acts as the main function
bool Server::OnInit()
{
    try
    {
        config.readFile(); //load configuration file on start
    }
    catch(configuration::FILE_ERR)
    {
        cout << "exception error reading file"<<endl;
    }
    catch(exception& e)
    {
        cout << "exception encountered "<<e.what()<<endl;
    }
    catch(...)
    {
        cout <<"exception encountered"<<endl;
    }

    ServerFrame* ServerWindow = new ServerFrame(wxT(TITLE));

    ServerWindow->Show(true);

    return true;
}

