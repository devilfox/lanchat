
#ifndef _WX_WX_H_ //avoiding multiple inclusions
#include"wx/wx.h"
#endif



#define USE_EXECUTE

#ifdef USE_EXECUTE
    #define EXEC(cmd) wxExecute((cmd), wxEXEC_SYNC)
#else
    #define EXEC(cmd) system(cmd)
#endif



class serverThreads;

class MainServer; //needs to declared here because we need to update the GUI also

class ClientHandler_Send;

class ClientHandler_Receive;

class ConfigurationDialog : public wxDialog
{
    private:
    wxString serv_name;
    wxString serv_ip;
    wxString serv_port;
    wxString bcast_ip;
    wxString bcast_rr;

    DECLARE_EVENT_TABLE()
public:
  ConfigurationDialog(const wxString& title);
  void onSave(wxCommandEvent& event);

};

enum
{
    ID_NAME,
    ID_SERIP,
    ID_BCASTIP,
    ID_BCASTREF,
    ID_PORT,
    ID_Save,
     ID_Quit = 1,
    ID_About,
    ID_Config,
    ID_Run,
    ID_Stop,
    ID_Details,
};

//event table//--------------------------------//
BEGIN_EVENT_TABLE(ConfigurationDialog, wxDialog)
EVT_BUTTON(ID_Save, ConfigurationDialog::onSave)
END_EVENT_TABLE()
//---------------------------------------//


class detailDialog:public wxDialog
{
    public:
    detailDialog(const wxString& title);
};

class ServerFrame: public wxFrame
{
protected:

    serverThreads* handle;
    serverThreads *CreateThread_Server();
    wxFont font;
    wxColor col;
public:
    int sfd;
    ServerFrame(const wxString& title);

    wxMenuBar *menubar;//declaration of menu bar
    wxMenu *file;
    wxMenu *setting;
    wxMenu *details;
    wxTextCtrl *updatePanel;

    void OnQuit(wxCloseEvent & event);
    void OnAbout(wxCommandEvent& event);
    void onConfig(wxCommandEvent& event);
    void OnRun(wxCommandEvent& event);
    void OnStop(wxCommandEvent& event);
    void onDetails(wxCommandEvent& event);

    void UpdateScreen(const wxString& text)
    {
        wxMutexGuiEnter(); //ensures that it is only thread running it...
        updatePanel->AppendText(text);
        wxMutexGuiLeave();
    }
    void ClearScreen()
    {
        wxMutexGuiEnter(); //ensures that it is only thread running it...
        updatePanel->Clear();
        wxMutexGuiLeave();
    }

    DECLARE_EVENT_TABLE()
};


//event table//--------------------------------//
BEGIN_EVENT_TABLE(ServerFrame, wxFrame)

EVT_MENU(ID_About, ServerFrame::OnAbout)

//EVT_MENU(ID_Quit, ServerFrame::OnQuit)

EVT_MENU(ID_Config, ServerFrame::onConfig)

EVT_MENU(ID_Run, ServerFrame::OnRun)

EVT_MENU(ID_Stop, ServerFrame::OnStop)

EVT_MENU(ID_Details, ServerFrame::onDetails)

END_EVENT_TABLE()
//---------------------------------------//


class serverThreads: public wxThread
{
    protected:
    ServerFrame* UIupdate;

    public:

    serverThreads(ServerFrame *frame){
        UIupdate = frame;
    }

    virtual void *Entry();

    virtual void OnExit();

};


class MainServer:public Socket  //implementing socket server class
{
protected:
    ServerFrame* UIupdate;
    char* current_client_ip;
    int current_client_port;
    ClientHandler_Send *CreateThread_Send(int fd);
    ClientHandler_Receive *CreateThread_Receive(int fd,char* IP);
public:
MainServer(ServerFrame* frame)
{
    UIupdate = frame;
}
    void CurrentClientDetails(const struct sockaddr_in remote_info);
    int ListenForConnections();
};

class ClientHandler_Send: public wxThread
{
protected:
    int fd;

    ServerFrame* UIupdate;
public:
    ClientHandler_Send(ServerFrame* frame,int FD)
    {
        UIupdate = frame;
        fd =FD;
    }
    virtual void *Entry();

    virtual void OnExit();
};


class ClientHandler_Receive: public wxThread
{
protected:
        int fd;
        char* ip;
        ServerFrame* UIupdate;
public:
    ClientHandler_Receive(ServerFrame* frame,int FD,char* IP)
    {
        UIupdate = frame;
        fd =FD;
        ip = new char[strlen(IP)+1];
        strcpy(ip,IP);
    }
    virtual void *Entry();

    virtual void OnExit();
};


ConfigurationDialog::ConfigurationDialog(const wxString & title): wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 400))
{

    //----------------------initialise the configuration values-----------------------------//

    serv_name = wxString::FromUTF8(config.getServerName());
    serv_ip = wxString::FromUTF8(config.getServerIP());
    serv_port=wxString::FromUTF8(config.getServerPORT());
    bcast_ip = wxString::FromUTF8(config.getBcastIP());
    bcast_rr = wxString::FromUTF8(config.getRefreshRate());
    //--------------------------------------------------------------------------------------//

  wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("configure server"),wxPoint(5, 5), wxSize(485, 310));

  /*----------------add items to the congiguration panel------------------------------------------------------*/
 // wxRadioButton *rb = new wxRadioButton(panel, -1,wxT("automatically detect own IP address"), wxPoint(15, 30), wxDefaultSize, wxRB_GROUP);

  wxRadioButton *rb1 = new wxRadioButton(panel, -1,wxT("use following IP settings"), wxPoint(15, 55));

    wxStaticText* description = new wxStaticText( this, wxID_STATIC,wxT("Enter the server details.\n\nLeaving blanks may \n\ncause errors."), wxPoint(340,125),wxDefaultSize, 0 );


  wxStaticText* descr = new wxStaticText( this, wxID_STATIC,wxT("IP address"), wxPoint(50,100),wxDefaultSize, 0 );
  wxTextCtrl *tc = new wxTextCtrl(panel, ID_SERIP, serv_ip,wxPoint(160, 100));

  wxStaticText* descr2 = new wxStaticText( this, wxID_STATIC,wxT("Use port no"), wxPoint(50,145),wxDefaultSize, 0 );
  wxTextCtrl *tc2 = new wxTextCtrl(panel, ID_PORT, serv_port,wxPoint(160, 145));

  //wxStaticText* descr3 = new wxStaticText( this, wxID_STATIC,wxT("Broadcast address "), wxPoint(50,160),wxDefaultSize, 0 );
  //wxTextCtrl *tc3 = new wxTextCtrl(panel, ID_BCASTIP, bcast_ip,wxPoint(160, 160));

  wxStaticText* descr4 = new wxStaticText( this, wxID_STATIC,wxT("Server name"), wxPoint(50,190),wxDefaultSize, 0 );
  wxTextCtrl *tc4 = new wxTextCtrl(panel, ID_NAME, serv_name,wxPoint(160, 190));

 // wxStaticText* descr5 = new wxStaticText( this, wxID_STATIC,wxT("Broadcast rate"), wxPoint(50,220),wxDefaultSize, 0 );
  //wxTextCtrl *tc5 = new wxTextCtrl(panel, ID_BCASTREF, bcast_rr,wxPoint(160, 220));

  wxButton *okButton = new wxButton(this,ID_Save, wxT("save"),wxDefaultPosition, wxSize(80, 30));


  hbox->Add(okButton, 1); //adds a add button

/*------------------------------------------------------------------------------------------------------------*/

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
  ShowModal();

  Destroy();
}



detailDialog::detailDialog(const wxString & title): wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 400))
{
     wxTextCtrl *updatePanel = new wxTextCtrl;
}


//constructor defination
ServerFrame::ServerFrame(const wxString& title):wxFrame(NULL, -1, title, wxPoint(window_pos_x, window_pos_y), wxSize(window_width, window_height),wxDEFAULT_FRAME_STYLE & ~ (wxRESIZE_BORDER | wxRESIZE_BOX | wxMAXIMIZE_BOX))
{
    font =  *wxITALIC_FONT;

        menubar = new wxMenuBar;
    file = new wxMenu;//menus in menu bar
    file->Append( ID_About, _("&About...") );
    file->AppendSeparator();
    file->Append( ID_Run, _("&Run server\tCtrl-R") );
    file->AppendSeparator();
    file->Append( ID_Stop, _("&Stop server\tCtrl-S") );
    file->AppendSeparator();
    file->Append( ID_Quit, _("&Exit\tCtrl-X") );
    menubar->Append(file, wxT("&File"));


    setting = new wxMenu;
    setting->Append(ID_Config,_("&configuration\tCtrl-C"));
    menubar->Append(setting, wxT("&Setting"));

    //details = new wxMenu;
    //details->Append(ID_Details,_("&Client Details"));
    //menubar->Append(details,wxT("&Details"));
    SetMenuBar(menubar);

    CreateStatusBar();
    SetStatusText( _("lanchat server") );
//----------------------------------------update panel for the server(monitor)--------------------------------------------------------------------//

    updatePanel = new wxTextCtrl(this, wxID_ANY, _T(""), wxPoint(0, 0), wxSize(window_width, window_height),wxTE_MULTILINE | wxTE_READONLY);

    updatePanel->SetFont(font);
    Connect(wxEVT_CLOSE_WINDOW , wxCloseEventHandler(ServerFrame::OnQuit));
    //updatePanel->SetForegroundColour(col);
    #ifdef __WIN32__
    updatePanel->WriteText(wxT("Running on Windows Variant\n\n\n"));
    #else
    updatePanel->WriteText(wxT("Running on Unix Variant\n\n"));
    #endif
    //---------------------------------------------------------------------------------------------------------------------------------------------//

}


