#ifndef FILES_H  //avoiding multiple inclusions
#define FILES_H

#ifndef FSTREAM_H
#define FSTREAM_H
#include <fstream>
#endif

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif


class configuration : public DataSecurity,public server_vars  //deriving from data security so as to access encrypt and decrpt routines
{
private:
    ifstream ConfigFileIN;
    ofstream ConfigFileOUT;
    char* path;
    bool canWritefile;


public:
        class FILE_ERR{};
        configuration():canWritefile(false){
        reset(); //reset cipher
        path = new char[strlen("datas\\import.config")+3];
        strcpy(path,"datas\\import.config");

    }
    bool readFile()
    {
    try
    {
        char* sname;
        char* sIP;
        char* sport;
        char* bip;
        char* brr;


        ConfigFileIN.open(path);
        if(!ConfigFileIN) throw FILE_ERR();
        //--read config file here
        ConfigFileIN>>serverName;
        ConfigFileIN>>serverIP;
        ConfigFileIN>>serverPort;
        ConfigFileIN>>bcastIP;
        ConfigFileIN>>bcastRr;
        sname = enc_algorithm(serverName);
        sIP = enc_algorithm(serverIP);
        sport = enc_algorithm(serverPort);
        bip = enc_algorithm(bcastIP);
        brr = enc_algorithm(bcastRr);
        memset(&serverName,0,sizeof serverName);
        memset(&serverIP,0,sizeof serverIP);
        memset(&serverPort,0,sizeof serverPort);
        memset(&bcastIP,0,sizeof bcastIP);
        memset(&bcastRr,0,sizeof bcastRr);

        strcpy(serverName,sname);
        strcpy(serverIP,sIP);
        strcpy(serverPort,sport);
        strcpy(bcastIP,bip);
        strcpy(bcastRr,brr);


        PORT = atoi(serverPort);
        ConfigFileIN.close();
        //cout<<serverName<<endl<<serverIP<<endl<<serverIP<<endl<<bcastIP<<endl<<bcastRr;
        cout<<"--loading configuration file......sucessful--"<<endl<<endl;
    }
    catch(exception& e)
    {
        throw e;
    }
    }
    bool writeFile(char* ser_names,char* ser_ips,char* ser_ports,char* bcast_ips,char* bcast_rates)
    {
        try
        {
                ConfigFileOUT.open(path);
                if(!ConfigFileOUT) throw FILE_ERR();
        //--writing codes to be here
        if(strlen(ser_names)<1)
            ConfigFileOUT<<"-"<<endl;
            else
                ConfigFileOUT<<enc_algorithm(ser_names)<<endl;

        if(strlen(ser_ips)<1)
            ConfigFileOUT<<"-"<<endl;
            else
                ConfigFileOUT<<enc_algorithm(ser_ips)<<endl;

        if(strlen(ser_ports)<1)
            ConfigFileOUT<<"-"<<endl;
            else
                ConfigFileOUT<<enc_algorithm(ser_ports)<<endl;

        if(strlen(bcast_ips)<1)
            ConfigFileOUT<<"-"<<endl;
            else
                ConfigFileOUT<<enc_algorithm(bcast_ips)<<endl;

        if(strlen(bcast_rates)<1)
            ConfigFileOUT<<"-"<<endl;
            else
                ConfigFileOUT<<enc_algorithm(bcast_rates)<<endl;
        //-----------------------------------------------//
                ConfigFileOUT.close();

            cout<<"---writing configuration file......sucessful---"<<endl;

                readFile(); //update the variables
        }
        catch(exception& e)
        {
            throw e;
        }

    }

void exception_handler(char* ser_names,char* ser_ips,char* ser_ports)
{
    try
    {

        strcpy(serverName,ser_names);
        strcpy(serverIP,ser_ips);
        strcpy(serverPort,ser_ports);
        //strcpy(bcastIP,bip);
        //strcpy(bcastRr,brr);


        PORT = atoi(serverPort);
    }
    catch(...)
    {
        throw;
    }
}
};

configuration config; //a configuraton object

#endif//FILES_H
