#ifndef SERVER_VARIABLES
#define SERVER_VARIABLES




//frame position defined as
#define window_pos_x 250
#define window_pos_y 250

//frame dimensions defined as
#define window_width 300
#define window_height 450

//window title defined as
#define TITLE "server"


bool isMainServerRunning=false;

int PORT=10101;  //defines the port on which server will be running
#define MAX_CLIENTS 10 //defines maximum number of clients each of the server can handle at a time
int MAX_LEN = 1024; //maximum lenght of accept string
const int MAXLEN = 1024 ;   // Max lenhgt of a message.

//includes all the configuration variables set..
class server_vars
{
protected:
       //--------------configuration read items----------------------//
    char serverName[50];
    char serverIP[50];
    char serverPort[20];
    char bcastIP[50];
    char bcastRr[5];
    //-----------------------------------------------------------//
public:
    char* getServerName()
    {
        return serverName;
    }
    char* getServerIP()
    {
        return serverIP;
    }
    char* getServerPORT()
    {
        return serverPort;
    }
    char* getBcastIP()
    {
        return bcastIP;
    }
    char* getRefreshRate()
    {
        return bcastRr;
    }

};

#endif


