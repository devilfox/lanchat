
#ifndef WX_FRAME
#define WX_FRAME



void ConfigurationDialog::onSave(wxCommandEvent& WXUNUSED(event))
{
    char* serverName;
    char* serverIP;
    char* broadcastIP;
    char* BcastRR;
    char* serverPort;
try
{

    wxString name;
    wxString servIP;
    wxString bcastIP;
    wxString port;
    wxString refresh;

    wxTextCtrl* server_name = (wxTextCtrl*) FindWindow(ID_NAME);
    wxTextCtrl* server_IP = (wxTextCtrl*) FindWindow(ID_SERIP);
    wxTextCtrl* server_BCASTIP = (wxTextCtrl*) FindWindow(ID_BCASTIP);
    wxTextCtrl* server_BCASTREF = (wxTextCtrl*) FindWindow(ID_BCASTREF);
    wxTextCtrl* server_PORT = (wxTextCtrl*) FindWindow(ID_PORT);

    name=server_name->GetValue();
    servIP=server_IP->GetValue();
   // bcastIP=server_BCASTIP->GetValue();
   // refresh=server_BCASTREF->GetValue();
    port=server_PORT->GetValue();

//---------------convert the user input values to the character array-------------------------//
    serverName=new char[strlen(name.mbc_str())+1];
    strcpy(serverName,name.mbc_str());

    serverIP = new char[strlen(servIP.mbc_str())+1];
    strcpy(serverIP,servIP.mbc_str());

    broadcastIP = new char[strlen(bcastIP.mbc_str())+1];
    strcpy(broadcastIP,bcastIP.mbc_str());

    BcastRR = new char[strlen(refresh.mbc_str())+1];
    strcpy(BcastRR,refresh.mbc_str());

    serverPort = new char[strlen(port.mbc_str())+1];
    strcpy(serverPort,port.mbc_str());
//-----------------------------------------------------------------------------------------------//

//---------------------------------write to the configuration file-------------------------------//
    config.writeFile(serverName,serverIP,serverPort,broadcastIP,BcastRR);
//-----------------------------------------------------------------------------------------------//

delete serverName;
delete serverIP;
delete serverPort;
delete broadcastIP;
delete BcastRR;
}
catch(configuration::FILE_ERR)
{
    cout<<"exception in writing to file"<<endl;
    config.exception_handler(serverName,serverIP,serverPort);

delete serverName;
delete serverIP;
delete serverPort;
delete broadcastIP;
delete BcastRR;
}
catch(...)
{
    cout <<"exception occured-unable to save settings"<<endl;

delete serverName;
delete serverIP;
delete serverPort;
delete broadcastIP;
delete BcastRR;
}
    Close();

}



serverThreads *ServerFrame::CreateThread_Server()
{
    serverThreads* thread = new serverThreads(this);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);
    handle = thread;
    return thread;
}

ClientHandler_Send* MainServer::CreateThread_Send(int fd)
{
    ClientHandler_Send *thread = new ClientHandler_Send(UIupdate,fd);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);

    return thread;
}
ClientHandler_Receive* MainServer::CreateThread_Receive(int fd,char* IP)
{
    ClientHandler_Receive *thread = new ClientHandler_Receive(UIupdate,fd,IP);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);

    return thread;
}


void ServerFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    SetStatusText( _("about") );

    wxMessageBox( _("chat server\ndeveloped by:\nBibek\nBhuwan\nBiswas"),_("About"),wxOK | wxICON_INFORMATION, this );

    SetStatusText( _("lanchat server") );
}
void ServerFrame::onDetails(wxCommandEvent& WXUNUSED(event))
{

}
void ServerFrame::OnQuit(wxCloseEvent & event)
{


     wxMessageDialog *dial = new wxMessageDialog(NULL , wxT("are you sure to quit?"), wxT("Confirm"), wxYES_NO | wxNO_DEFAULT);
        if(dial->ShowModal()== wxID_YES)
        {

            if(isMainServerRunning)
            {
                handle->Kill();
                #ifdef __WIN32__
                closesocket(sfd);
                WSACleanup(); //release the winsock dll library
                #else
                close(sfd);
                #endif
               // handle->Kill(); //kill the server thread to stop the server
            }

            Destroy();
        }

        else event.Veto();
}
void ServerFrame::onConfig(wxCommandEvent& WXUNUSED(event))
{
if(!isMainServerRunning){

    SetStatusText( _("lanchat server configuration") ); //change the status text


    ConfigurationDialog *custom = new ConfigurationDialog(wxT("server configuration panel"));
    custom->Show(true);

    SetStatusText( _("lanchat server") ); //reset the status text
}
else{
         wxMessageBox( _("server running..cannot edit configuration file!"),_("server::message"),wxOK | wxICON_INFORMATION, this );
}

}



//-------------------------core server functions-------------------------------------------------------------//
void ServerFrame::OnRun(wxCommandEvent& WXUNUSED(event))
{
    if(!isMainServerRunning)
    {
        serverThreads* thread = CreateThread_Server();
        thread->Run();
        wxString update1 = wxString::Format(wxT("server listening @ %i"),PORT);
        SetStatusText( update1 ); //reset the status text
        isMainServerRunning = true;
        updatePanel->Clear();
    }
    else
    {
         wxMessageBox( _("server already running"),_("server::message"),wxOK | wxICON_INFORMATION, this );
        //cout <<"---server already running---"<<endl<<endl;
    }

}
void ServerFrame::OnStop(wxCommandEvent& WXUNUSED(event))
{
    if(isMainServerRunning)
    {

    handle->Kill(); //kill the server thread to stop the server
    #ifdef __WIN32__
    closesocket(sfd);
    WSACleanup(); //release the winsock dll library
    #else
    close(sfd);
    #endif

    updatePanel->Clear(); //clear the screen
    isMainServerRunning = false;
    SetStatusText( _("server Offline") ); //reset the status text
    //cout<<"server offline"<<endl<<endl;
    }
    else{
     wxMessageBox( _("No running servers!!"),_("server::message"),wxOK | wxICON_INFORMATION, this );
    }

}
//-------------------------------------------------------------------------------------------------------------//



void* serverThreads::Entry()
{
    try
    {

    UIupdate->UpdateScreen(wxT("Creating Socket\n"));
    MainServer server(UIupdate);

    if(UIupdate->sfd=server.CreateSocket(PORT)<0) UIupdate->UpdateScreen(wxT("Error creating Socket\n"));
    else UIupdate->UpdateScreen(wxT("Socket created sucessfully \n"));
        cout <<"server listening for connections"<<endl;
    server.ListenForConnections();
    }
    catch(...)
    {
        isMainServerRunning=false;
        wxMessageBox( _("unable to start server!!"),_("server::message"),wxOK | wxICON_INFORMATION, UIupdate );
    }

}
void serverThreads::OnExit()
{

}


void MainServer::CurrentClientDetails(const struct sockaddr_in remote_info)
{

    try
    {

    //-----------------------------receives current client informations-----------------------------//
    current_client_ip = new char[strlen(inet_ntoa(remote_info.sin_addr))+1];
    strcpy(current_client_ip,inet_ntoa(remote_info.sin_addr));
    current_client_port = ntohs(remote_info.sin_port);
    }
    catch(exception& e)
    {
        cout <<"exception occurred "<<e.what();
    }
//------------------------------------------------------------------------------------------------
}

int MainServer::ListenForConnections()
{
isMainServerRunning = true;

    UIupdate->UpdateScreen(wxT("--------------------------------------------\nserver: "));
    UIupdate->UpdateScreen(wxString::FromUTF8(host));
    UIupdate->UpdateScreen(wxT("\n--------------------------------------------\n\n"));
    wxString update1 = wxString::Format(wxT("server listening on port: %i \n\n"),PORT);
    UIupdate->UpdateScreen(update1);


    int flag; //error flag
    flag = listen(serverfd, MAX_CLIENTS); // enanble listen

	if(flag < 0){
         //cout << "error :" << strerror(errno) << endl<<endl;
         return -1 ;
	}
	else {
        cout <<"---Listening to socket.....sucessful---"<<endl<<endl;

	}

    char ipstr[46];
    int port;
    int new_sd;

    struct sockaddr_in remote_info ;
    #ifdef __WIN32__
    int addr_size;
    #else
    socklen_t addr_size;
    #endif

    addr_size = sizeof(remote_info);

    //cout <<"Waiting for clients to connect...."<<endl<<endl;
    UIupdate->UpdateScreen(wxT("Waiting for clients to connect to server... \n\n"));

while(1)
{
 try
    {
    new_sd = accept(serverfd, (struct sockaddr *) &remote_info, &addr_size);

    getpeername(new_sd, (struct sockaddr*)&remote_info, &addr_size);

    memset(&current_client_ip,0,sizeof current_client_ip);


    CurrentClientDetails(remote_info);

    UIupdate->UpdateScreen(wxT("SERVER: connection accepted from: "));
    UIupdate->UpdateScreen(wxString::FromUTF8(current_client_ip));
    UIupdate->UpdateScreen(wxT("\n"));

    ClientHandler_Send * Sthread = CreateThread_Send(new_sd);
    Sthread->Run();
    ClientHandler_Receive* Rthread = CreateThread_Receive(new_sd,current_client_ip);
    Rthread->Run();
/*
    #ifdef __WIN32__
    closesocket(new_sd);
    #else
    close(new_sd);
    #endif
    */
    }
    catch(...)
    {

        cout <<"exception occured while running server";
    }
}
    return new_sd;
    }




void* ClientHandler_Send::Entry()
{


}
void ClientHandler_Send::OnExit()
{

}
void* ClientHandler_Receive::Entry()
{
    Socket* OndemandSender = new Socket;
    incoming HandleReceive(ip,fd,OndemandSender); //create a object that will handle receives from client and creates a database of its own

    wxString IP = wxString::FromUTF8(ip);

    char buf[MAXLEN];
    int buflen;
    int wfd;
    bool isquit=false;

    while(!isquit)
    {
        try
        {


        //read incomming message.
        buflen = recv(fd, buf, MAXLEN-1,0);
        if (buflen <= 0)
        {

            wxString update1 = wxString::Format(wxT("\nSERVER: client  ID:%i  IP:"),database.getID(fd));
            wxString update2 = wxString::Format(wxT(" disconnected \n\nSERVER: clearing file descriptor %i\n\n"),fd);
             UIupdate->UpdateScreen(update1);
             UIupdate->UpdateScreen(IP);
             UIupdate->UpdateScreen(update2);

            isquit = true;
            //cout << "---client disconnected. Clearing fd. " << fd <<"---"<< endl<<endl ;

            database.removeClient(database.getID(fd));//clearing the database of that client
            #ifdef __WIN32__
            closesocket(fd);
            #else
            close(fd);
            #endif

        }
        else
        {
            //cout << buf<<endl<<endl;
            HandleReceive.parseReceivedPacket(pstrings(buf));
            HandleReceive.clear();
        }
        memset(&buf,0,sizeof(buf));
        }
        catch(incoming::CMD_ERR)
        {
            cout <<"exception while parsing "<<endl;
        }
        catch(...)
        {
            cout << "exception occured"<<endl;
        }
    }

}
void ClientHandler_Receive::OnExit()
{

}

#endif
