#ifndef SOCKET_IMPLEMENT_H
#define SOCKET_IMPLEMENT_H
#ifdef __WIN32__ //if code is being compiled under windows

#ifndef _WINSOCK_H
#include <winsock2.h>
#endif
//to be undefined from windows.h
#undef CreateDialog
#undef _UNICODE
#undef CreateFont
#undef CreateWindow
#undef LoadMenu
#undef FindText
#undef GetCharWidth
#undef FindWindow
#undef PlaySound
#undef GetClassName
#undef GetClassInfo
#undef LoadAccelerators
#undef DrawText
#undef LoadIcon
#undef LoadBitmap
//---------------------------------//
#else //if code is running on unix varient
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

class Socket
{
protected:
     int serverfd;
     char* host;
public:
    class SOCK_ERR{};
    char* GetHostName();
    int Send(int fd, string data);
    int Receive(int fd);
    int CreateSocket(int port);
};


char* Socket::GetHostName()
{
try
{
    char ch[50];
    size_t len;
    len = strlen(ch);
    if(gethostname(ch,sizeof(ch))==-1) {
        //cout << "error getting host name"<<endl<<endl;
        return NULL;
    }
    else{
            host = new char(strlen(ch)+1);
        strcpy(host,ch);
        //cout<<"server: "<<host<<endl<<endl;
        return host;
    }
}
catch(exception& e)
{
    throw SOCK_ERR();
}
}
int Socket::Send(int fd, string data)
{
    //cout << "--sending data to file descriptor -> "<< fd << "---"<<endl<<endl;
    int ret;
    ret = send(fd, data.c_str(), strlen(data.c_str()),0);
    if(ret != strlen(data.c_str()))
    {
       //cout <<"error sending data"<<endl<<endl;
       return -1;
    }
    return 0;
}

int Socket::Receive(int fd)
{
    char buf[MAXLEN];
    int buflen;
    int wfd;

    for(;;)
    {
        //read incomming message.
        buflen = recv(fd, buf, MAXLEN-1,0);
        if (buflen <= 0)
        {
            //cout << "client disconnected. Clearing fd. " << fd << endl<<endl ;
            #ifdef __WIN32__
            closesocket(fd);
            #else
            close(fd);
            #endif

        }
        else
        {
            //cout<<buf;
        }
    }
}

int Socket::CreateSocket(int port)
{
    #ifdef __WIN32__  //windows specific code
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(1,1), &wsaData) != 0)
    {
        //cout<<"error starting socket class"<<endl<<endl;
        exit(-1);
    }
    else
    {
        //cout<<"---loading winsock2 library.....sucessful---"<<endl<<endl;
    }
    #endif

    int flag;    //for free address function call
    char yes = 1; //same as above

    struct sockaddr_in SERVER_ADDRESS; // struct to store server adress and ports
    memset(&SERVER_ADDRESS,0, sizeof(SERVER_ADDRESS)); //setting struct to zero making sure its empty

	SERVER_ADDRESS.sin_family      = AF_INET; //IPV4
	SERVER_ADDRESS.sin_addr.s_addr = htonl(INADDR_ANY); //multihoming
	SERVER_ADDRESS.sin_port        = htons(port); //providing a port to connect on

    serverfd = socket(AF_INET, SOCK_STREAM, 0); // create a IPV4 socket

    if(serverfd < 0){
        //cout <<"error creating socket"<<endl<<endl;
        exit(-1);
    }
    else {
        //cout<<"---creating socket....sucessful---"<<endl<<endl;
    }

    flag = setsockopt(serverfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)); //prevent address already in use

	flag = bind(serverfd, (struct sockaddr *) &SERVER_ADDRESS, sizeof(SERVER_ADDRESS)); // binding the address with port

	if(flag != 0){
         //cout << "error :" << strerror(errno) << endl<<endl;
         exit(-1);
	}
	else //cout <<"---binding socket....sucessful---"<<endl<<endl;
	GetHostName();

    return serverfd;
}

Socket mainsocket;

#endif
