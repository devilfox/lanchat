#ifndef COMMANDPROCESSOR_H
#define COMMANDPROCESSOR_H


#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif


enum task {CLIDET,CLINOS,SELFID,MSGREC,MSGSEN,IMCLNO,AUTHEN,AUSTAT,NONE}; //defines the set of task to be done

class incoming
{
protected:
    Socket* onDemand; //this object will be used to send data to the designated client
    pstrings ip;
    int fd;
    pstrings flag; //flag defines the task to be done
    pstrings packet;
    task define_task; //enum data to signify the task
    bool protectBuffer; //packet to make the command used before it gets reaccesssed
                        //should free the buffer once the command is invoked

    //---------for client ID process---------------------------------------//
    pstrings client_name;
    //----------------------------------------------------------------------//
    //-----used for sending client details----------------------------------//
    pstrings client_details;
    //----------------------------------------------------------------------//
    //---used for client sending message to the client_no--//
    pstrings client_no;

    int client_id; //parsed version of client_no string
    //-----------------------------------------------------//
    void client_details_resolver(); //gets client name
    void msg_sending_details_resolver(); //executes MSGSEN command
    pstrings client_database_resolver(); //executes CLIDET command
    void send_clientNos(); //executes CLINOS command
    void send_ownclientID();
    void parse_remote();
    void p_authen();
public:
class CMD_ERR{};
//    incoming():protectBuffer(false),define_task(NONE){}
    incoming(pstrings IP,int FD, Socket* ptr):protectBuffer(false),define_task(NONE){
        fd = FD;
        onDemand = ptr;
        ip = IP;
    }

    bool parseReceivedPacket(pstrings received);
    void freeBuffer();
    void clear() //clears all the memory for refilling in it...should be called when another instance is to be created
    {
        try
        {
                 ip.clear();
                 flag.clear();
                 packet.clear();
                 client_name.clear();
                 client_details.clear();
                 client_no.clear();
    }
        catch(...)
        {
            throw CMD_ERR();
        }
    }

};
void incoming::client_details_resolver()
{
    try
    {
        pstrings cln_name;
        cln_name = packet.getString(10,packet.find(']')-1);

    //create the record------------------------------------------//
    database.addClient(cln_name.c_str(),ip,fd);
    //---------------------------------------------------------//
    }
    catch(exception& e)
    {
        throw CMD_ERR();
    }
}
void incoming::msg_sending_details_resolver()
{
    try{

    pstrings message;



   client_no = packet.getString(10,packet.find(')')-1);

   int client_id = client_no.Toint();

    message = packet.getString(packet.find('[')+1,packet.find(']')-1);

pstrings IDholder;
IDholder.Tostring(database.getID(fd));

pstrings standard_msg;

standard_msg + "|$MSGREC$(";
standard_msg + IDholder;
standard_msg + ")[";
standard_msg + message;
standard_msg + "]|";

//--------------------after parsing message and requested client..send data to the destination client--------------//

    onDemand->Send(database.getfd(client_id),standard_msg.Tostring()); //get fd from database and send the message;
//-----------------------------------------------------------------------------------------------------------------//
    }
    catch(...)
    {
        throw CMD_ERR();
    }
}

void incoming::freeBuffer()
{
    if(protectBuffer)   protectBuffer = false;
}

pstrings incoming::client_database_resolver() //this is the function that will be used to send the client details
{
    try
    {


        pstrings details;
        ClientsDataBase copybaseData(database);

        if(copybaseData.getTotalClients()<2) { //if there is less than 2 clients means the client is alone that is connected to server
        ////cout <<"no any other clients currently connected!!"<<endl<<endl;
        onDemand->Send(fd,string("|$SIGEND$|")); //signal the end of sending client details
            return pstrings(" ");
        }



        for(int i =0 ;i<copybaseData.getTotalClients();i++) //loop the total clients
        {
            details.clear(); //clear the contents
            details = copybaseData.getDetailedClientString(i);
            ////cout << details<<endl<<endl;
//cout <<"resolver 2"<<endl;
            if(copybaseData.getFDbyPos(i)!=fd)
            {
                onDemand->Send(fd,details.Tostring()); //send the requested details

            }
            Sleep(30);//current 10ms---might need some delay cause the clients should parse the sent data...keep this in mind


//cout <<"resolver 3"<<endl;
        }
        onDemand->Send(fd,string("|$SIGEND$|")); //signal the end of sending client details
        return pstrings(" ");
    }
    catch(...)
    {
        throw CMD_ERR();
    }
}

void incoming::send_clientNos()
{
    try
    {
        pstrings standardString;
        pstrings clnNos;
        clnNos.Tostring(database.getTotalClients());

        standardString + "|$CLINOS$(";
        standardString + clnNos;
        standardString + ")|";

        onDemand->Send(fd,standardString.Tostring()); //send back the details

    }
    catch(...)
    {
        throw CMD_ERR();
    }
}
void incoming::send_ownclientID()
{
    try
    {
        pstrings standardString;
        pstrings clnNo;

        clnNo.Tostring(database.getID(fd));

        standardString + "|$IMCLNO$(";
        standardString + clnNo;
        standardString + ")|";

        onDemand->Send(fd,standardString.Tostring()); //send back the details
    }
    catch(...)
    {
        cout <<"handled"<<endl;
    }
}

bool incoming::parseReceivedPacket(pstrings received)
{
    try
    {

        //cout << received <<endl;

            int flags=0; // 1 if asked details, 2 if sendmessagee is received,, anything else it is 0
            clear();
            packet = received;

                if(received[0]!='|' || received[received.length()-1]!='|')
                {
                    //cout<<"command syntax incomplete | | missing"<<endl;
                    return -1;
                }

                if(received[1]!='$' || received[8]!='$')
                {
                    //cout<<"invalid command syntax $command$ missing"<<endl;
                    return -1;
                }

        //if syntax and command count is good then
            if(protectBuffer) //check if the buffer is freed
            {
                //cout << "buffer protected, cannot write to buffer!"<<endl;
                return -1;
            }



        protectBuffer = true;
        //-------------------resolve the task------------------------------------------------//

                 if(received.isMatching(2,"CLIDET",7)) define_task = CLIDET;    //requested client details
            else if(received.isMatching(2,"CLINOS",7)) define_task = CLINOS;    //requested client numbers
            else if(received.isMatching(2,"SELFID",7)) define_task = SELFID;    //sending client details to server
            else if(received.isMatching(2,"MSGREC",7)) define_task = MSGREC;    //requested server/client to receive data
            else if(received.isMatching(2,"MSGSEN",7)) define_task = MSGSEN;    //requested server to send data to specified address
            else if(received.isMatching(2,"IMCLNO",7)) define_task = IMCLNO;    //requested requesting clients' number
            else if(received.isMatching(2,"AUTHEN",7)) define_task = AUTHEN;    //authenticate the password and username
            else if(received.isMatching(2,"AUSTAT",7)) define_task = AUSTAT;    //set authentication status


            else //if task is not resolved
            {
                //cout <<"unknown task!"<<endl;
                define_task = NONE; //no defined task to be done
                return false;
            }

        //-----------------------------------------------------------------------------------//
        protectBuffer = false;


        switch(define_task)
        {
            case CLIDET: //should be a function that sends back the client details to the client that requested
            ////cout<<"command received : REQUESTED CLIENT DETAILS"<<"   sender: "<<database.getID(fd)<<endl<<endl;
            client_database_resolver();
            break;

            case CLINOS: //should be a fn that sends back the no of connected clients to the client that requested
            ////cout<<"command received : REQUESTED NO OF CLIENTS"<<"   sender: "<<database.getID(fd)<<endl<<endl;
            send_clientNos();
            break;

            case SELFID:  //this identification leads to the entry of client in Database
            ////cout<<"command received : REQUESTED REGISTER IDENTITY"<<"   sender: "<<database.getID(fd)<<endl<<endl;
            client_details_resolver();
            break;

            case MSGREC:
            break;

            case MSGSEN: //send message
            ////cout<<"command received : REQUESTED SEND MESSAGE"<<"   sender: "<<database.getID(fd)<<endl<<endl;
            msg_sending_details_resolver();
            break;

            case IMCLNO: //send own client number
            ////cout<<"command received : REQUESTED OWN ID"<<"   sender: "<<database.getID(fd)<<endl<<endl;
            send_ownclientID();
            break;

            case AUTHEN:
            parse_remote();
            break;

            case AUSTAT:
            p_authen();
            break;

        }


        return true;
    }
    catch(...)
    {
        throw CMD_ERR();
    }
}

void incoming::parse_remote()
{

    //receive as |$AUTHEN$(cid)[ID]{PSWD}<acton>|
    //send as    |$AUTHEN$(ID)[PASS]<ACT>|
try
{


    pstrings CID;
    pstrings ID;
    pstrings PASS;
    pstrings ACT;
    int cid_int;
    pstrings msg;

            CID = packet.getString(packet.find('(')+1,packet.find(')')-1);
            cid_int = CID.Toint();
            ID = packet.getString(packet.find('[')+1,packet.find(']')-1);
            PASS = packet.getString(packet.find('{')+1,packet.find('}')-1);
            ACT = packet.getString(packet.find('<')+1,packet.find('>')-1);

    pstrings IDholder;
    IDholder.Tostring(database.getID(fd));

    msg + "|$AUTHEN$(";
    msg + IDholder;
    msg + ")[";
    msg + ID;
    msg + "]{";
    msg + PASS;
    msg + "}<";
    msg + ACT;
    msg + ">|";

    int fd = database.getfd(cid_int);

    if(fd!=-1)
    {
        onDemand->Send(fd,msg.Tostring());
    }

}
catch(exception& e)
{
        cout << "exection handled"<<endl;
}
catch(...)
{
    cout <<"handled"<<endl;
}

}
void incoming::p_authen()
{
    try
    {
        pstrings smsg;
        pstrings RID;
        pstrings res;
        int sid_int;

            RID = packet.getString(packet.find('(')+1,packet.find(')')-1);
            sid_int = RID.Toint();

            res = packet.getString(packet.find('<')+1,packet.find('>')-1);

            smsg + "|$AUSTAT$<";
            smsg + res;
            smsg + ">|";

            onDemand->Send(database.getfd(sid_int),smsg.Tostring());

    }
    catch(exception& e)
    {
        cout <<"exception handled"<<endl;
    }
    catch(...)
    {
        cout <<"exception handled"<<endl;
    }
}




#endif
