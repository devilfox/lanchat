
#ifndef CLIENTDATABASEHANDLER_H
#define CLIENTDATABASEHANDLER_H

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef VECTOR_H
#define VECTOR_H
#include <vector>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif

class ClientsDataBase
{
protected:

bool writing;
bool readingName;
bool readingIp;
bool readingFd;
bool removing;
bool readingID;

    vector<string> clients_name;
    vector<string> clients_ip;
    vector<int> clients_fd;
    vector<int> clients_id;
    int clients_count;
    void signal()
    {
        Socket sender;
        string sig("|$SIGREF$|"); //signal refresh
        for(int i=0;i<clients_id.size();i++)
        {
            sender.Send(clients_fd[i],sig);
            Sleep(5); //give a sleep of 5ms;
        }
    }
public:
class DB_ERR{}; //exception class
ClientsDataBase():writing(false),readingName(false),readingIp(false),readingFd(false),removing(false),clients_count(0),readingID(false){}

    int addClient(char* name, char* ip, int fd); //returns assigned client ID
    int removeClient(int id);
    int getfd(int id);
    char* getName(int id);
    char* getIp(int id);
    int getID(int fd);
    pstrings getDetailedClientString(int i); //return character array in format to send details to clients for the position i;
    int getFDbyPos(int i)
    {
        return clients_fd[i];
    }
    int getTotalClients()
    {
        return clients_fd.size();
    }
        bool isreading(){
        if(readingFd||readingIp||readingName) return true;
        else return false;
    }
    bool iswriting(){
        return writing;
    }
    bool isremoving(){
        return removing;
    }

};

int ClientsDataBase::addClient(char* name, char* ip, int fd)
{

    writing = true;
    bool exists=false;

    for(int i=0;i<clients_fd.size();i++)
    {
        if(clients_fd[i]==fd)
        {
            //cout <<"client already exists"<<endl<<endl;
            string temp(name);
            clients_name[i]=temp;
            exists = true;

        }
    }

if(!exists){ //if new record is to be created

    ++clients_count;
    //cout << "New client added to database.. name: "<<name<<"IP: "<<ip<<"FD: "<<fd<<"ID: "<<clients_count<<endl<<endl;

    string temp1(name);
    string temp2(ip);

    clients_name.push_back(temp1);
    clients_ip.push_back(temp2);
    clients_fd.push_back(fd);
    clients_id.push_back(clients_count);
    signal(); //signals all clients connected about the addition of clients;
    return clients_count;
}
writing = false;

}

int ClientsDataBase::removeClient(int id)
{
removing = true;
    for(int i=0;i<clients_id.size();i++)
    {
        if(clients_id[i]==id)
        {
            //cout <<clients_id[i]<< "erasing client..name: "<<clients_name[i]<<"IP: "<<clients_ip[i]<<"ID: "<<clients_id[i]<<endl<<endl;
            clients_name.erase(clients_name.begin()+i);
            clients_ip.erase(clients_ip.begin()+i);
            clients_id.erase(clients_id.begin()+i);
            clients_fd.erase(clients_fd.begin()+i);
            signal(); //signals all clients connected about the addition of clients;
            return 0;
            break;
        }
    }
removing = false;
    //else
    return -1;
}

int ClientsDataBase::getfd(int id)
{

    readingFd = true;
    for(int i=0l;i<clients_id.size();i++)
    {
        if(clients_id[i]==id)
        {
            return clients_fd[i];
        }
    }
readingFd = false;
    //else if not found
    return -1; //-1 is to represent error or not found condition
}

char* ClientsDataBase::getName(int id)
{
    try
    {

        readingName = true;
        char* temp;
        for(int i=0;i<clients_name.size();i++)
        {
            if(clients_id[i]==id)
            {
                temp = new char[strlen(clients_name[i].c_str())+1];
                strcpy(temp,clients_name[i].c_str());
                return temp;
            }
        }
        readingName= false;
        //else if not found
        return NULL; //-1 is to represent error or not found condition
    }
    catch(exception& e)
    {
        throw DB_ERR();
    }
}

char* ClientsDataBase::getIp(int id)
{
    try
    {
        readingIp = true;
        char* temp;
        for(int i=0;i<clients_ip.size();i++)
        {
            if(clients_id[i]==id)
            {
                temp = new char[strlen(clients_ip[i].c_str())+1];
                strcpy(temp,clients_ip[i].c_str());
                return temp;
            }
        }
        readingIp = false;
        //else if not found
        return NULL; //-1 is to represent error or not found condition
    }
       catch(exception& e)

    {
        throw DB_ERR();
    }
}

int ClientsDataBase::getID(int fd)
{
     readingID = true;
    for(int i=0l;i<clients_fd.size();i++)
    {
        if(clients_fd[i]==fd)
        {
            return clients_id[i];
        }
    }
readingID = false;
    //else if not found
    return -1; //-1 is to represent error or not found condition
}

pstrings ClientsDataBase::getDetailedClientString(int i)
{
try
{
    if(clients_id.size()>=i+1)
    {

    pstrings buffer;
    pstrings charID;
    charID.Tostring(clients_id[i]);

//-------------------------------create a formatted string-----------------------------------------------//
    buffer + "|$CLIDET$[";
    buffer + clients_name[i];
    buffer + "](";
    buffer + charID;
    buffer + ")|";


    return buffer;
    }
}
catch(exception& e)
{
    throw DB_ERR();
}
}

ClientsDataBase database;








#endif
