#ifndef SERVER_VARIABLES
#define SERVER_VARIABLES

#include <cstring>


#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif



#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif



//frame position defined as
#define window_pos_x 250
#define window_pos_y 100

//frame dimensions defined as
#define window_width 800
#define window_height 600

//window title defined as
#define TITLE "client"
#define MAX_CLIENTS 10 //defines maximum number of clients each of the server can handle at a time



namespace GV //global variables
{
bool isConnected=false;
bool isClientRunning=false;

bool canSendCommand=false;
int PORT=10101;  //defines the port on which server will be running

const int MAX_LEN = 1024; //maximum lenght of accept string
const int MAXLEN = 1024 ;   // Max lenhgt of a message.
//includes all the configuration variables set..
int current_id_selected=0;
int backup_id;
}

namespace wxVars
{
    bool quit = false; //used in receive thread;
    bool iswaiting = false;
    bool soundFlag = true;
    const int List_Font_size = 11; //to show the online list
    const int msg_update_font_size = 10;
    bool GUIsend=false;
    bool GUIreceive = false;
    bool isbeingused = false;
}

namespace threadVars
{
    bool isClientThreadRunning = false;
    bool isReceiveThreadRunning = false;
    bool isSendThreadRunning = false;
}
namespace authentication
{
    bool checked = false;
    int sender;
    bool authenticate = false;
    int action = 0;
}

class client_vars
{
protected:
       //--------------configuration read items----------------------//
    char clientName[50];
    char serverIP[50];
    char serverPort[20];
    int port;
    char remote_id[50];
    char pswd[50];

    //-----------------------------------------------------------//
public:
client_vars()
{
    cout <<"constructor of client vars called"<<endl;
}
    char* getClientName()
    {
        return clientName;
    }
    char* getServerIP()
    {
        return serverIP;
    }
    char* getServerPORT()
    {
        return serverPort;
    }
    int getServerPORTi()
    {
        return port;
    }
    char* getRemoteID()
    {
        return remote_id;
    }
    char* getPassword()
    {
        return pswd;
    }
~client_vars()
{
    cout <<"destructor of client vars called"<<endl;
}
};


class xtra
{
    private:
    int flag;
    char* msg;
    public:
    xtra()
    {
        cout <<"constructor of xtra called"<<endl;
        msg = new char[1];
        msg[0]='\0';
    }
    void getxtra(char* get,int flg)
    {
        msg = new char[strlen(get)+1];
        strcpy(msg,get);

        flag = flg;
    }
    int getFlag()
    {
        return flag;
    }
    char* getMsg()
    {
        return msg;
    }
    ~xtra()
    {
        cout<<"destructor of xtra called"<<endl;
        delete msg;
    }
};

xtra dbx;






#endif


