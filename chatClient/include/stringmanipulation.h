#ifndef STRINGMANIPULATION_H
#define STRINGMANIPULATION_H

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef _WX_WX_H_ //avoiding multiple inclusions
#include"wx/wx.h"
#endif


#ifndef VECTOR_H
#define VECTOR_H
#include <vector>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif


class stringHandler
{
    private:
    //----------used for processing the text that is to be sent------//
    pstrings Message;
    pstrings To;
    pstrings snd_msg;
    //---------------------------------------------------------------//

    pstrings From;

    vector<int>get;

    public:
    class STR_ERR{};
    stringHandler()
    {
        cout<<"constructor of stringhandler called\n"<<endl;

    }
    //----------used for processing the text that is to be sent------//
    void clear()
    {
        Message.clear();
        To.clear();
        snd_msg.clear();
        From.clear();
    }
    void processString(pstrings);
    void processString2(pstrings read, pstrings from);
    string getMessageForSending();
    wxString getToMessageForUpdate();
    wxString getMessageForUpdate();
    wxString getReceivedMsg();
    wxString getFrom();
    //---------------------------------------------------------------//
    ~stringHandler()
    {
        cout <<"destructor of stringHandler called"<<endl;

    }
};

void stringHandler::processString(pstrings read)
{
    try
    {
        clear();
        pstrings xtra;
        pstrings buffers;

        pstrings buffer;
        pstrings temp;
        pstrings msgHolder;

        pstrings msg;
        pstrings hi;

        get.clear();
        xtra.clear();

        xtra = database.getName(GV::current_id_selected);
        pstrings rid;
        rid = rid.Tostring(GV::current_id_selected);
        buffers+"To ";
        buffers+xtra;
        buffers+":  ";

        buffers.toUpper();

        hi = read;

        int s_count = hi.concurrence(hi.length()-1,'\n');


        if(s_count!=0)
        {
            int pos = hi.length()-(s_count);
            hi.breakpos(pos);
        }
        msg = hi;

        bool flag=false;
        for(int i=0;i<hi.length();i++)
        {
            if(hi[i]=='\n'){ get.push_back(i);
                flag=true;
            }
        }
                int size = get.size()+3;

    msgHolder.clear();
        if(flag)
        {
            flag=false;
            int counts=0;
            int j=0;
            for(int i=0;i<get.size();i++)
            {
                counts++;
                buffer.clear();
                buffer = hi.getString(j,get[i]-1);
                if(counts==0){
                        msgHolder + buffer;
                        msgHolder + "\n";
                        msgHolder.append(buffers.length(),' ');
                }

                else
                {
                    msgHolder + buffer;
                    msgHolder + "\n";
                    msgHolder.append(buffers.length(),' ');
                }

                        j=get[i]+1;
                        //getch();
                }
                buffer.clear();
                buffer = hi.getString(j,hi.length()-1);
                msgHolder + buffer;

            }
                    else
                {
                    msgHolder.clear();
                    msgHolder + hi;
                }

                    msg.encrypt();
                    pstrings standard_msg;
                    standard_msg + "|$MSGSEN$(";
                    standard_msg + rid;
                    standard_msg + ")[";
                    standard_msg + msg;
                    standard_msg + "]|";


            Message = msgHolder;
            To = buffers;
            snd_msg = standard_msg;

    }
    catch(exception& e)
    {
         cout <<"exception"<<endl;
    }
    catch(...)
    {
        cout <<"exception"<<endl;


    }
}

void stringHandler::processString2(pstrings read, pstrings from)
{
    try
    {
        read.decrypt();
        clear();
        pstrings buffers;
        pstrings buffer;
        pstrings temp;
        pstrings msgHolder;
        pstrings msg;
        pstrings hi;
        vector<int>get;
        get.clear();
        buffers+"From ";
        buffers+from;
        buffers+":  ";

        buffers.toUpper();
        From = buffers;




        hi = read;

        int s_count = hi.concurrence(hi.length()-1,'\n');


        if(s_count!=0)
        {
            int pos = hi.length()-(s_count);
            hi.breakpos(pos);
        }
        msg = hi;

        bool flag=false;
        for(int i=0;i<hi.length();i++)
        {
            if(hi[i]=='\n'){ get.push_back(i);
                flag=true;
            }
        }
                int size = get.size()+3;

    msgHolder.clear();
        if(flag)
        {
            flag=false;




            int counts=0;
            int j=0;
            for(int i=0;i<get.size();i++)
            {
                counts++;
                buffer.clear();
                buffer = hi.getString(j,get[i]-1);
                if(counts==0){
                        msgHolder + buffer;
                        msgHolder + "\n";
                        msgHolder.append(buffers.length(),' ');
                }

                else
                {
                    msgHolder + buffer;
                    msgHolder + "\n";
                    msgHolder.append(buffers.length(),' ');
                }

                        j=get[i]+1;
                        //getch();
                }
                buffer.clear();
                buffer = hi.getString(j,hi.length()-1);
                msgHolder + buffer;

            }
                    else
                {
                    msgHolder.clear();
                    msgHolder + hi;
                }



                     Message = msgHolder;
                     Message + "\n\n";






    }
    catch(exception& e)
    {
         cout <<"exception"<<endl;
    }
    catch(...)
    {
        cout <<"exception"<<endl;


    }
}

string stringHandler::getMessageForSending()
{
    return string(snd_msg.c_str());
}
wxString stringHandler::getToMessageForUpdate()
{
    return wxString::FromUTF8(To.c_str());
}
wxString stringHandler::getMessageForUpdate()
{
    return wxString::FromUTF8(Message.c_str());
}
wxString stringHandler::getReceivedMsg()
{
    return wxString::FromUTF8(Message.c_str());;
}
wxString stringHandler::getFrom()
{
    return wxString::FromUTF8(From.c_str());
}

stringHandler forRecvMsg;


#endif
