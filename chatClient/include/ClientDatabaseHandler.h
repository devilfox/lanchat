
#ifndef CLIENTDATABASEHANDLER_H
#define CLIENTDATABASEHANDLER_H

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef VECTOR_H
#define VECTOR_H
#include <vector>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif

class ClientsDataBase:public Socket
{
protected:

bool writing;
bool readingName;
bool removing;
bool readingID;
string temp1;

public:
    class DB_ERR{};
    vector<string> clients_name;
    vector<int> clients_id;
    int clients_count;

ClientsDataBase():writing(false),readingName(false),removing(false),clients_count(0),readingID(false)
{
    cout<<"constructor of clients database invoked"<<endl;
}

    int addClient(char* name, int id); //returns assigned client ID
    int removeClient(int id);
    char* getName(int id);
    int refreshDatabase(int sockfd); //calls the socketfd defined to load the clients
    char* getClientByOrder(int ord)
    {
        try
        {
                char* temp = new char[strlen(clients_name[ord].c_str())+1];
            strcpy(temp,clients_name[ord].c_str());
            return temp;
        }
        catch(exception& e)
        {
            cout << "handled"<<endl;
        }

    }
    void clear()
    {
        clients_name.clear();
        clients_id.clear();
        memset(&clients_name,0,sizeof clients_name);
        memset(&clients_id,0,sizeof clients_id);

    }

    int getTotalClients()
    {
        return clients_name.size();
    }
        bool isreading(){
        if(readingName) return true;
        else return false;
    }
    bool iswriting(){
        return writing;
    }
    bool isremoving(){
        return removing;
    }
    int getIDbyName(char* name)
    {
        for(int i=0;i<clients_name.size();i++)
        {
            if(strcmp(name,clients_name[i].c_str())==0)
            return clients_id[i];
        }
    }
    bool refreshSelectedID(int id)
    {
        for(int i=0;i<clients_id.size();i++)
        {
            if(id==clients_id[i])
                return true;

        }

        return false;
    }
    ~ClientsDataBase()
    {
        cout <<"destructor of clients database called"<<endl;

    }
};

int ClientsDataBase::addClient(char* name, int ids)
{
    int id = ids;

    writing = true;
    bool exists=false;

    for(int i=0;i<clients_name.size();i++)
    {
        if(clients_id[i]==id)
        {
            //cout <<"client already exists"<<endl<<endl;
            exists = true;
        }
    }

if(!exists){ //if new record is to be created

//cout << "adding a client"<<endl;
    ++clients_count;
    //temp1.clear();
    temp1=name;
    clients_name.push_back(temp1);
    clients_id.push_back(id);
    return clients_count;
}
writing = false;

}

int ClientsDataBase::removeClient(int id)
{
removing = true;
    for(int i=0;i<clients_id.size();i++)
    {
        if(clients_id[i]==id)
        {

            clients_name.erase(clients_name.begin()+i);
            clients_id.erase(clients_id.begin()+i);

            return 0;
            break;
        }
    }
removing = false;
    //else
    return -1;
}



char* ClientsDataBase::getName(int id)
{
    readingName = true;
    char* temp;
    for(int i=0;i<clients_name.size();i++)
    {
        if(clients_id[i]==id)
        {
            temp = new char[strlen(clients_name[i].c_str())+1];
            strcpy(temp,clients_name[i].c_str());
            return temp;
        }
    }
    readingName= false;
    //else if not found
    return NULL; //-1 is to represent error or not found condition

}

int ClientsDataBase::refreshDatabase(int fd)
{

}


ClientsDataBase database;








#endif
