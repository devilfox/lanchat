#ifndef FILES_H  //avoiding multiple inclusions
#define FILES_H

#ifndef FSTREAM_H
#define FSTREAM_H
#include <fstream>
#endif

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif


class configuration : public DataSecurity,public client_vars  //deriving from data security so as to access encrypt and decrpt routines
{
private:
    ifstream ConfigFileIN;
    ofstream ConfigFileOUT;
    char* path;
    bool canWritefile;


public:
        class FILE_ERR{}; //exception class
        configuration():canWritefile(false)
        {
            cout<<"constructor of file section invoked"<<endl;
        reset(); //reset cipher
        path = new char[strlen("datas\\import.config")+3];
        strcpy(path,"datas\\import.config");

    }
    bool readFile()
    {
        try
        {
                char* sname;
                char* sIP;
                char* sport;
                char* bip;
                char* brr;
                char* id;
                char* pwd;

                ConfigFileIN.open(path);
                if(!ConfigFileIN) throw FILE_ERR();

                //--read config file here
                ConfigFileIN>>clientName;
                ConfigFileIN>>serverIP;
                ConfigFileIN>>serverPort;
                ConfigFileIN>>remote_id;
                ConfigFileIN>>pswd;

                sname = enc_algorithm(clientName);
                sIP = enc_algorithm(serverIP);
                sport = enc_algorithm(serverPort);
                id = enc_algorithm(remote_id);
                pwd = enc_algorithm(pswd);

                memset(&clientName,0,sizeof clientName);
                memset(&serverIP,0,sizeof serverIP);
                memset(&serverPort,0,sizeof serverPort);
                memset(&remote_id,0,sizeof remote_id);
                memset(&pswd,0,sizeof pswd);


                strcpy(clientName,sname);
                strcpy(serverIP,sIP);
                strcpy(serverPort,sport);
                strcpy(remote_id,id);
                strcpy(pswd,pwd);

                port = atoi(serverPort);
                GV::PORT = port;
                ConfigFileIN.close();
                ////cout<<clientName<<endl<<serverIP<<endl<<serverIP<<endl<<bcastIP<<endl<<bcastRr;
                //cout<<"--loading configuration file......sucessful--"<<endl<<endl;
                delete sname;
                delete sIP;
                delete sport;
                delete bip;
                delete brr;
                delete id;
                delete pwd;

        }
        catch(exception& e)
        {
            cout <<"exception occured while reading file::"<<e.what()<<endl;
            throw e;
        }
    }
    bool writeFile(char* cli_name,char* ser_ips,char* ser_ports,char* rem_id,char* pswd)
    {

        ConfigFileOUT.open(path);
        if(!ConfigFileOUT) throw FILE_ERR();
//--writing codes to be here
if(strlen(cli_name)<1)
    ConfigFileOUT<<"-"<<endl;
    else
        ConfigFileOUT<<enc_algorithm(cli_name)<<endl;

if(strlen(ser_ips)<1)
    ConfigFileOUT<<"-"<<endl;
    else
        ConfigFileOUT<<enc_algorithm(ser_ips)<<endl;

if(strlen(ser_ports)<1)
    ConfigFileOUT<<"-"<<endl;
    else
        ConfigFileOUT<<enc_algorithm(ser_ports)<<endl;

if(strlen(rem_id)<1)
    ConfigFileOUT<<"-"<<endl;
    else
        ConfigFileOUT<<enc_algorithm(rem_id)<<endl;

if(strlen(pswd)<1)
    ConfigFileOUT<<"-"<<endl;
    else
        ConfigFileOUT<<enc_algorithm(pswd)<<endl;


//-----------------------------------------------//
        ConfigFileOUT.close();

    //cout<<"---writing configuration file......sucessful---"<<endl;

        readFile(); //update the variables
    }

    void execp_handler(char* cli_name,char* ser_ips,char* ser_ports);
    ~configuration()
    {
        cout<<"destructor of file section called"<<endl;
        delete path;
    }

};

void configuration::execp_handler(char* cli_name,char* ser_ips,char* ser_ports)
{
    try
    {

                strcpy(clientName,cli_name);
                strcpy(serverIP,ser_ips);
                strcpy(serverPort,ser_ports);


                port = atoi(serverPort);
                GV::PORT = port;
    }
    catch(...)
    {
        throw FILE_ERR();
    }
}

configuration config; //a configuraton object

#endif//FILES_H
