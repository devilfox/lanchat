#ifndef WXCLASSES_H
#define WXCLASSES_H


#ifndef _WX_WX_H_ //avoiding multiple inclusions
#include"wx/wx.h"
#endif

#include "wx/sound.h"

#define USE_EXECUTE

#ifdef USE_EXECUTE
    #define EXEC(cmd) wxExecute((cmd), wxEXEC_SYNC)
#else
    #define EXEC(cmd) system(cmd)
#endif

#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/app.h"
    #include "wx/frame.h"
    #include "wx/menu.h"
    #include "wx/msgdlg.h"
    #include "wx/icon.h"
    #include "wx/textctrl.h"
    #include "wx/filedlg.h"
#endif

#include "wx/sound.h"
#include "wx/numdlg.h"
#include "wx/textdlg.h"

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------



#define WAV_FILE _T("effects\\incoming.wav")
#define icon _T("effects\\imperials.ico")
//#define ICON _T("effects\\imperials.xpm")

enum
{
    ID_Quit = 1,
    ID_About,
    ID_Config,
    ID_Details,
    ID_Message,
    ID_Write,
    ID_Send,
    ID_RENAME = 1,
    ID_LISTBOX = 5,
     ID_NAME,
    ID_CLN,
    ID_PORT,
    ID_BCASTREF,
    ID_SIP,
    ID_Save,
    ID_RID,
    ID_PWD,
    ID_PWD2,
    ID_GID,
    ID_GPWD,
    ID_RM,
    ID_SHUT,
    ID_RST,
    ID_LGF,
    ID_HIB,
    ID_DONE,
    ID_TGS,
    ID_CHK,
};


class ClientThreads;

class MainClient; //needs to declared here because we need to update the GUI also

class ClientHandler_Send;

class ClientHandler_Receive;

class Helper;

class remote;

class extras;


class ConfigurationDialog : public wxDialog
{
    private:
    wxString client_name;
    wxString serv_ip;
    wxString serv_port;
    wxString bcast_ip;
    wxString bcast_rr;
    wxString remote_id;
    wxString remote_pswd;

    DECLARE_EVENT_TABLE()
public:
  ConfigurationDialog(const wxString& title);
  void onSave(wxCommandEvent& event);
    ~ConfigurationDialog()
    {
        cout<<"destructor of configuration dialog called"<<endl;
    }
};

//event table//--------------------------------//
BEGIN_EVENT_TABLE(ConfigurationDialog, wxDialog)
EVT_BUTTON(ID_Save, ConfigurationDialog::onSave)
END_EVENT_TABLE()
//---------------------------------------//


class detailDialog:public wxDialog
{
    public:
    detailDialog(const wxString& title);
};

class OnlinePanel : public wxPanel
{
public:
    OnlinePanel(wxPanel *parent);
    wxListBox *onlineList;


    void addOnlineClient(char* name)
    {
        wxString OnlineClient;
        OnlineClient = wxString::FromUTF8(name);
        onlineList->Append(OnlineClient);
    }
    void clear()
    {
        onlineList->Clear();
    }
};



class ClientFrame: public wxFrame,public stringHandler
{
protected:
    wxString    m_soundFile;
    ClientThreads* handle;
    ClientHandler_Send* Shandle;
    ClientHandler_Receive* Rhandle;
    ClientThreads *CreateThread_Client(OnlinePanel*);
    bool CreateSound(wxSound& snd) const
    {
        return snd.Create(m_soundFile);
    }
public:
    extras* create_thread(xtra*);
    ClientFrame(const wxString& title);
    void OnDblClick(wxCommandEvent& event);
    wxListBox *listbox;
    OnlinePanel *online;
    wxTextCtrl *update_message;
    wxTextCtrl *write_message;
    wxTextCtrl *sendto;
    wxTextCtrl *OLflag;
    wxSound* incoming;

    wxMenuBar *menubar;//declaration of menu bar
    wxMenu *file;
    wxMenu *setting;
    wxMenu *details;


    void OnKey(wxKeyEvent& event);
    void onsend_control();
    void SetMsgUpdateColor(int);
    void PlaySound();


    void OnQuit(wxCloseEvent & event);
    void toogleSound(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void onConfig(wxCommandEvent& event);
    void onRun(wxCommandEvent& event);
    void onSend(wxCommandEvent& event);
    void onClose(wxCommandEvent& event);
    void onRM(wxCommandEvent& event);
    void updateMsgBox(wxString str)
    {
        if(wxVars::isbeingused) while(wxVars::isbeingused){}
        wxVars::isbeingused = true;
        update_message->AppendText(str);
        wxVars::isbeingused = false;

    }




    void updateSendto(char* to)
    {
        wxString msg = wxString::FromUTF8(to);
        sendto->Clear();
        sendto->WriteText(msg);
    }
    void ClearSendto()
    {
        sendto->Clear();
    }
    void flagON()
    {
        OLflag->Clear();
        OLflag->WriteText(wxT("Server: Connected"));
    }
    void flagOFF()
    {
        OLflag->Clear();
        OLflag->WriteText(wxT("Server: Disconnected"));
    }
    void setShandle(ClientHandler_Send* ptr)
    {
        Shandle = ptr;
    }
    void setRhandle(ClientHandler_Receive* ptr)
    {
        Rhandle = ptr;
    }
    void setHandle(ClientThreads* ptr)
    {
        handle = ptr;
    }
    DECLARE_EVENT_TABLE()
    ~ClientFrame()
    {
        cout<<"destructor of clientframe called"<<endl;
    }
};

void ClientFrame::SetMsgUpdateColor(int flag)
{
    wxColour color;
    if(flag==0) //i.e set the receive color
         color.Set(255,0,0); //red;
    else if(flag==1) color.Set(0,0,255);//blue;


    update_message->SetForegroundColour(color);

}


//event table//--------------------------------//
BEGIN_EVENT_TABLE(ClientFrame, wxFrame)

EVT_MENU(ID_About, ClientFrame::OnAbout)

EVT_MENU(ID_RM, ClientFrame::onRM)

EVT_MENU(ID_Quit, ClientFrame::onClose)

EVT_MENU(ID_TGS, ClientFrame::toogleSound)

EVT_KEY_DOWN(ClientFrame::OnKey)

EVT_MENU(ID_Config, ClientFrame::onConfig)

EVT_MENU(ID_Details, ClientFrame::onRun)

EVT_BUTTON(ID_Send, ClientFrame::onSend)

END_EVENT_TABLE()
//---------------------------------------//



class remote: public wxDialog
{
    private:
    int selected_client;

    public:
    remote(const wxString& title);
    void onDone(wxCommandEvent& event);
     DECLARE_EVENT_TABLE()
     ~remote()
     {
         cout <<"destructor of remote called"<<endl;
     }
};

//event table//--------------------------------//
BEGIN_EVENT_TABLE(remote, wxDialog)
EVT_BUTTON(ID_DONE, remote::onDone)
END_EVENT_TABLE()
//---------------------------------------//

class ClientThreads: public wxThread
{
    protected:
    ClientFrame* UIupdate;
    Helper* Help;
    OnlinePanel* online;
    ClientHandler_Send *CreateThread_Send(int fd);
    ClientHandler_Receive *CreateThread_Receive(int fd,ClientHandler_Send* ptr);
    public:

    ClientThreads(ClientFrame *frame, Helper* hptr,OnlinePanel* ptr){

        UIupdate = frame;
        Help = hptr;
        online= ptr;
        UIupdate->setHandle(this);
    }

    virtual void *Entry();

    virtual void OnExit();

};





class MainClient:public Socket  //implementing socket server class
{
protected:
    ClientFrame* UIupdate;
    char* current_client_ip;
    int current_client_port;

public:
MainClient(ClientFrame* frame)
{
    UIupdate = frame;
    current_client_ip = new char[1];
    current_client_ip[0]='\0';
}
~MainClient()
{
    delete current_client_ip;
}

};





class ClientHandler_Send: public wxThread
{
protected:
    int fd;
    Helper* ondemand;
    ClientFrame* UIupdate;
    void askDetails()
    {
        #ifdef __WIN32__
        Sleep(5);
        #else
        sleep(1/500);
        #endif

    database.clear();
    while(!GV::canSendCommand)
    {
        //cout << "waiting"<<endl;
    }
    string command("|$CLIDET$|");
    mainSocket.Send(fd,command);
    }
public:
    ClientHandler_Send(ClientFrame* frame,int FD,Helper* ptr)
    {

        UIupdate = frame;
        fd =FD;
        ondemand = ptr;
        UIupdate->setShandle(this);
    }
    virtual void *Entry();

    virtual void OnExit();
};








class ClientHandler_Receive: public wxThread
{
protected:
        ClientHandler_Send* handle;
        int fd;
        ClientFrame* UIupdate;
        Helper* ondemand;
        OnlinePanel* OLlist;
public:
    ClientHandler_Receive(ClientFrame* frame,int FD,ClientHandler_Send* ptr,Helper* hptr,OnlinePanel* optr)
    {
        handle = ptr;
        UIupdate = frame;
        fd =FD;
        ondemand = hptr;
        OLlist = optr;
        UIupdate->setRhandle(this);
    }
    virtual void *Entry();

    virtual void OnExit();
};




class extras:public wxThread
{
    private:
    ClientFrame* frame;
    xtra* xt;
    wxString msg;
    int flag;

    public:
    extras(ClientFrame* ptr,xtra* p)
    {
        frame = ptr;
        xt = p;
        //flag 1 means run a  message box
    }
    virtual void *Entry();

    virtual void OnExit();
};






class Helper // this class will help us sync between the socket handlers, database and GUI update mechanisms
{
    private:
    ClientFrame* GUI; // this pointer will help enabling smooth GUI using same object
    ClientsDataBase* db;
    OnlinePanel* updateOnlinePanel;

    public:
    Helper(ClientFrame* ptr,OnlinePanel* panel){

        GUI = ptr;
        updateOnlinePanel = panel;
    }
    void messageUpdater(char* msg)
    {
    try
    {
        GUI->SetMsgUpdateColor(0);
        GUI->PlaySound();

        while(wxVars::GUIsend){}
        wxVars::GUIreceive = true;
        GUI->updateMsgBox(forRecvMsg.getFrom());
        GUI->updateMsgBox(forRecvMsg.getReceivedMsg());
        wxVars::GUIreceive = false;
    }
    catch(stringHandler::STR_ERR)
    {
        cout <<"exception handled"<<endl;
    }
    catch(...)
        {
            cout <<"exception occured while updating message"<<endl;
        }
    }
    void refreshHelperDatas(){
        db = new ClientsDataBase(database); // create a new instance using the

    }
    void refreshOnlineList()
    {
        refreshHelperDatas();
        updateOnlinePanel->clear();
////cout<<"--->>> "<<db->getTotalClients()<<endl;
       for(int i=0;i<db->getTotalClients();i++)
       {
        updateOnlinePanel->addOnlineClient(db->getClientByOrder(i));
       }
  //     //cout <<endl<<endl;


    }
};







ConfigurationDialog::ConfigurationDialog(const wxString & title): wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 400))
{
    cout <<"constructor of configuration dialog called"<<endl;

    //----------------------initialise the configuration values-----------------------------//

    client_name = wxString::FromUTF8(config.getClientName());
    serv_ip = wxString::FromUTF8(config.getServerIP());
    serv_port=wxString::FromUTF8(config.getServerPORT());
    remote_id = wxString::FromUTF8(config.getRemoteID());
    remote_pswd = wxString::FromUTF8(config.getPassword());
    //--------------------------------------------------------------------------------------//

  wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("configure client connection settings"),wxPoint(5, 5), wxSize(485, 310));

  /*----------------add items to the congiguration panel------------------------------------------------------*/
 // wxRadioButton *rb = new wxRadioButton(panel, -1,wxT("automatically detect own IP address"), wxPoint(15, 30), wxDefaultSize, wxRB_GROUP);


  wxStaticText* descr = new wxStaticText( this, wxID_STATIC,wxT("Client Name"), wxPoint(50,60),wxDefaultSize, 0 );
  wxTextCtrl *tc = new wxTextCtrl(panel, ID_CLN, client_name,wxPoint(160, 60));

  wxStaticText* descr2 = new wxStaticText( this, wxID_STATIC,wxT("Server IP"), wxPoint(50,90),wxDefaultSize, 0 );
  wxTextCtrl *tc2 = new wxTextCtrl(panel, ID_SIP, serv_ip,wxPoint(160, 90));

  wxStaticText* descr3 = new wxStaticText( this, wxID_STATIC,wxT("Port no"), wxPoint(50,120),wxDefaultSize, 0 );
  wxTextCtrl *tc3 = new wxTextCtrl(panel, ID_PORT, serv_port,wxPoint(160, 120));



    wxCheckBox* cbox = new wxCheckBox( panel,ID_CHK, _T("&enable remote authentication"), wxPoint(50,170) );
    cbox->SetToolTip( _T("enables or disables remote authentication"));
    if(authentication::checked) cbox->SetValue(true);


  wxStaticText* descr4 = new wxStaticText( this, wxID_STATIC,wxT("remote id"), wxPoint(50,220),wxDefaultSize, 0 );
  wxTextCtrl *tc4 = new wxTextCtrl(panel,ID_RID,remote_id,wxPoint(160, 220),wxSize(150,20));

  wxStaticText* descr5 = new wxStaticText( this, wxID_STATIC,wxT("password"), wxPoint(50,250),wxDefaultSize, 0 );
  wxTextCtrl *tc5 = new wxTextCtrl(panel,ID_PWD, remote_pswd,wxPoint(160, 250),wxSize(150,20),wxTE_PASSWORD);

  wxStaticText* descr6 = new wxStaticText( this, wxID_STATIC,wxT("re-enter password"), wxPoint(50,280),wxDefaultSize, 0 );
  wxTextCtrl *tc6 = new wxTextCtrl(panel,ID_PWD2, remote_pswd,wxPoint(160, 280),wxSize(150,20),wxTE_PASSWORD);



  wxButton *okButton = new wxButton(this,ID_Save, wxT("save"),wxDefaultPosition, wxSize(80, 30));


  hbox->Add(okButton, 1); //adds a add button

/*------------------------------------------------------------------------------------------------------------*/

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
  ShowModal();

  Destroy();
}



detailDialog::detailDialog(const wxString & title): wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 400))
{
     wxTextCtrl *updatePanel = new wxTextCtrl;
}



//constructor defination
ClientFrame::ClientFrame(const wxString& title):wxFrame(NULL, -1, title, wxPoint(window_pos_x, window_pos_y), wxSize(window_width, window_height),wxDEFAULT_FRAME_STYLE & ~ (wxRESIZE_BORDER | wxRESIZE_BOX | wxMAXIMIZE_BOX))
{


    wxIcon ICON(icon, wxBITMAP_TYPE_ICO, 32,32 );
    this->SetIcon(ICON);


    cout <<"constructor of clientframe called"<<endl;
    m_soundFile = WAV_FILE;

   wxFont font;
   wxColor color;
   color.Set(wxT("#B7BDBB"));






    wxPanel * panel = new wxPanel(this, -1);

        menubar = new wxMenuBar;
    file = new wxMenu;//menus in menu bar
    file->Append( ID_About, _("&About...") );
    file->AppendSeparator();
    file->Append( ID_Quit, _("&Exit\tShift-X") );
    menubar->Append(file, wxT("&File"));


    setting = new wxMenu;
    setting->Append(ID_Config,_("&configuration\tShift-C"));
    menubar->Append(setting, wxT("&Setting"));

    details = new wxMenu;
    details->Append(ID_Details,_("&put online\tShift-O"));
    #ifdef __WIN32__
    details->Append(ID_RM,wxT("&remote control\tShift-M"));
    #endif
    details->Append(ID_TGS,wxT("&toogle sound\tShift-S"));

    menubar->Append(details,wxT("&Client"));

    SetMenuBar(menubar);


    wxStaticBox *st = new wxStaticBox(panel, -1, wxT("Online clients"),wxPoint(10, 1), wxSize(150, 510));

    wxColour Listcolor(72, 28, 28);
    wxFont Listfont(wxVars::List_Font_size, wxFONTFAMILY_ROMAN, -1, wxBOLD,false);
    listbox = new wxListBox(panel, ID_LISTBOX, wxPoint(20, 20), wxSize(130, 485));
    listbox->SetBackgroundColour(color);
    listbox->SetForegroundColour(Listcolor);
    listbox->SetFont(Listfont);




    wxFont Upmsg(wxVars::msg_update_font_size, wxFONTFAMILY_ROMAN, -1, wxLIGHT,false);
    update_message = new wxTextCtrl(panel, wxID_ANY, wxT(""), wxPoint(220, 80), wxSize(450, 250),wxTE_MULTILINE | wxTE_READONLY);
    update_message->SetFont(Upmsg);


    write_message  = new wxTextCtrl(panel, ID_Write, wxT(""), wxPoint(320, 395), wxSize(380, 80),wxTE_MULTILINE);
    write_message->Connect(ID_Write,wxEVT_KEY_DOWN , wxKeyEventHandler(ClientFrame::OnKey),NULL,this);
    write_message->SetFocus();

    sendto = new wxTextCtrl(this, -1, wxT(""),wxPoint(320, 370),wxSize(150,20), wxTE_READONLY);
    OLflag = new wxTextCtrl(this, -1, wxT(""),wxPoint(220, 55),wxSize(115,20), wxTE_READONLY);

    sendto->SetBackgroundColour(color);
    OLflag->SetBackgroundColour(color);
    online = new OnlinePanel(panel);

    wxButton *okButton = new wxButton(panel,ID_Send, wxT("send"),wxPoint(620,480), wxSize(80, 30));

    Connect(wxEVT_COMMAND_LISTBOX_DOUBLECLICKED,wxCommandEventHandler(ClientFrame::OnDblClick));
    Connect(wxEVT_CLOSE_WINDOW , wxCloseEventHandler(ClientFrame::OnQuit));

    CreateStatusBar();
    SetStatusText( _("lanchat client") );


    wxColour col1;
    col1.Set(wxT("#666666"));
    panel->SetBackgroundColour(col1);
        flagOFF();
}

remote::remote(const wxString& title): wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(500, 400))
{
    cout<<"constructor of remote called"<<endl;
    selected_client = GV::current_id_selected;
    wxString curr;
    curr = wxString::FromUTF8(config.getClientName());

     wxPanel *panel = new wxPanel(this, -1);

  wxBoxSizer *vbox = new wxBoxSizer(wxVERTICAL);
  wxBoxSizer *hbox = new wxBoxSizer(wxHORIZONTAL);

  wxStaticBox *st = new wxStaticBox(panel, -1, wxT("remote control panel"),wxPoint(5, 5), wxSize(485, 310));

  /*----------------add items to the congiguration panel------------------------------------------------------*/
 // wxRadioButton *rb = new wxRadioButton(panel, -1,wxT("automatically detect own IP address"), wxPoint(15, 30), wxDefaultSize, wxRB_GROUP);



    //wxStaticText* description = new wxStaticText( this, wxID_STATIC,wxT("Enter the Client connection details.\n\nLeaving blanks may \n\ncause errors."), wxPoint(300,125),wxDefaultSize, 0 );
    wxStaticText* descr1 = new wxStaticText( this, wxID_STATIC,wxT("selected client"), wxPoint(50,60),wxDefaultSize,0);
    wxTextCtrl *tc1 = new wxTextCtrl(panel,-1,curr,wxPoint(160, 60),wxSize(150,20),  wxTE_READONLY );


  wxStaticText* descr4 = new wxStaticText( this, wxID_STATIC,wxT("remote id"), wxPoint(50,90),wxDefaultSize,0);
  wxTextCtrl *tc4 = new wxTextCtrl(panel,ID_GID,wxT(""),wxPoint(160, 90),wxSize(150,20));

  wxStaticText* descr5 = new wxStaticText( this, wxID_STATIC,wxT("password"), wxPoint(50,120),wxDefaultSize, 0 );
  wxTextCtrl *tc5 = new wxTextCtrl(panel,ID_GPWD,wxT(""),wxPoint(160, 120),wxSize(150,20),wxTE_PASSWORD);


    wxRadioButton *rb1;
    wxRadioButton *rb2;
    wxRadioButton *rb3;
    wxRadioButton *rb4;


    rb1 = new wxRadioButton(panel, ID_SHUT,wxT("Shutdown"), wxPoint(60, 180));
    rb1->SetValue(true);
    rb2 = new wxRadioButton(panel, ID_RST,wxT("Restart"), wxPoint(60, 210));
    rb3 = new wxRadioButton(panel, ID_LGF,wxT("Logoff"), wxPoint(60, 240));

    #ifndef __WIN32__
    rb4 = new wxRadioButton(panel, ID_HIB,wxT("Hibernate"), wxPoint(60, 270));
    #endif



  wxButton *okButton = new wxButton(this,ID_DONE, wxT("done"),wxDefaultPosition, wxSize(80, 30));


  hbox->Add(okButton, 1); //adds a add button

/*------------------------------------------------------------------------------------------------------------*/

  vbox->Add(panel, 1);
  vbox->Add(hbox, 0, wxALIGN_CENTER | wxTOP | wxBOTTOM, 10);

  SetSizer(vbox);

  Centre();
  ShowModal();

  Destroy();

}


#endif
