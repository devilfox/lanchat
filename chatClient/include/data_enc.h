#ifndef DATA_ENC_H  //preventing multiple inclusions
#define DATA_ENC_H



#ifndef VECTOR_H
#define VECTOR_H
#include <vector>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif




class DataSecurity
{
protected:
    vector<unsigned char> key;
    int startBit;


public:
    class DAT_ERR{};
    DataSecurity():startBit(-19)
    {
        cout <<"constructor of datasecutity invoked"<<endl;
    } //set default value of start bit to 99
    void reset()
    {
        startBit=-19;
    }
    void clear()
    {
        key.clear();
    }
    char* enc_algorithm(char* dat)
    {
        try
        {
                 clear();
        reset();
        char* temp = new char[strlen(dat)+10];
        strcpy(temp,dat);
        key.push_back(startBit);
//generate the mask keys for each character in the string
        for(int i=1;i<strlen(dat);i++){
            key.push_back(startBit+i*2);
            startBit++;
        }
//now encrypt the string passed using the key;
        int i=0;
        for(i=0;i<strlen(dat);i++)
        {
            temp[i]^=key[i];
        }
        temp[i]='\0';
        return temp;

        }


    catch(exception& e)
    {
        cout << "Handled"<<endl;
    }
    catch(...)
    {
        throw DAT_ERR();
    }
}

~DataSecurity()
{
    cout << "destructor of datasecurity called"<<endl;
}
};















#endif//data_enc_h
