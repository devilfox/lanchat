#ifndef WX_FRAME
#define WX_FRAME
#include <conio.h>

#ifndef _WX_WX_H_ //avoiding multiple inclusions
#include"wx/wx.h"
#endif



#define USE_EXECUTE

#ifdef USE_EXECUTE
    #define EXEC(cmd) wxExecute((cmd), wxEXEC_SYNC)
#else
    #define EXEC(cmd) system(cmd)
#endif






void ConfigurationDialog::onSave(wxCommandEvent& WXUNUSED(event))
{

    char* serverName;
    char* serverIP;
    char* serverPort;
    char* remoteid;
    char* pass;
try
{
    wxString name;
    wxString servIP;
    wxString bcastIP;
    wxString port;
    wxString refresh;
    wxString rmid;
    wxString pswd;
    wxString pswd2;


    wxTextCtrl* client_name = (wxTextCtrl*) FindWindow(ID_CLN);
    wxTextCtrl* server_IP = (wxTextCtrl*) FindWindow(ID_SIP);
    wxTextCtrl* server_PORT = (wxTextCtrl*) FindWindow(ID_PORT);
    wxTextCtrl* r_id = (wxTextCtrl*) FindWindow(ID_RID);
    wxTextCtrl* pswds = (wxTextCtrl*) FindWindow(ID_PWD);
    wxTextCtrl* pswds2 = (wxTextCtrl*) FindWindow(ID_PWD2);
    wxCheckBox* chk = (wxCheckBox*) FindWindow(ID_CHK);

    name=client_name->GetValue();
    servIP=server_IP->GetValue();
    port=server_PORT->GetValue();
    rmid = r_id->GetValue();
    pswd = pswds->GetValue();
    pswd2 = pswds2->GetValue();

    authentication::checked = chk->GetValue();


    if(strcmp(pswd.mb_str(),pswd2.mb_str())==0) //if the passwords match
    {

//---------------convert the user input values to the character array-------------------------//
    serverName=new char[strlen(name.mbc_str())+1];
    strcpy(serverName,name.mbc_str());

    serverIP = new char[strlen(servIP.mbc_str())+1];
    strcpy(serverIP,servIP.mbc_str());

    serverPort = new char[strlen(port.mbc_str())+1];
    strcpy(serverPort,port.mbc_str());

    remoteid = new char[strlen(rmid.mb_str())+1];
    strcpy(remoteid,rmid.mb_str());

    pass = new char[strlen(pswd.mb_str())+1];
    strcpy(pass,pswd.mb_str());
//-----------------------------------------------------------------------------------------------//

//---------------------------------write to the configuration file-------------------------------//

    config.writeFile(serverName,serverIP,serverPort,remoteid,pass);
//-----------------------------------------------------------------------------------------------//

    delete serverName;
    delete serverIP;
    delete serverPort;
    delete remoteid;
    delete pass;

     Close();
    }
    else
    {
                wxMessageBox( _("passwords dont match"),_("!!"),wxOK | wxICON_INFORMATION, this );
    }
}
catch(configuration::FILE_ERR)
{
    cout << "exception occured handling file Output--establishing temporary placement of input values"<<endl;
    config.execp_handler(serverName,serverIP,serverPort);
    //exception form this protion will not be handled...hence program will terminate
    //if the values even cannot be stored in the temporary storage allocated
     Close();
}
catch(exception& e)
{
    cout<<"exception occurred: "<<e.what();
    delete serverName;
    delete serverIP;
    delete serverPort;
     Close();
}
catch(...)
{
    cout << "handled"<<endl;
    delete serverName;
    delete serverIP;
    delete serverPort;
     Close();
}

}




OnlinePanel::OnlinePanel(wxPanel * parent)
       : wxPanel(parent, wxID_ANY)
{


  ClientFrame *lb = (ClientFrame *) parent->GetParent();
  onlineList = lb->listbox;



}

ClientThreads *ClientFrame::CreateThread_Client(OnlinePanel* ptr)
{
    Helper * help = new Helper(this,online); //create an object of helper class
    ClientThreads* thread = new ClientThreads(this,help,ptr); //also pass the created helper class
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);
    handle = thread;
    return thread;
}

ClientHandler_Send* ClientThreads::CreateThread_Send(int fd)
{
    ClientHandler_Send *thread = new ClientHandler_Send(UIupdate,fd,this->Help);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);

    return thread;
}
ClientHandler_Receive* ClientThreads::CreateThread_Receive(int fd, ClientHandler_Send* ptr)
{
    ClientHandler_Receive *thread = new ClientHandler_Receive(UIupdate,fd,ptr,this->Help,this->online);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);

    return thread;
}

extras* ClientFrame::create_thread(xtra* p)
{
    extras *thread = new extras(this,p);
    if ( thread->Create() != wxTHREAD_NO_ERROR )
    {
        wxLogError(wxT("Can't create thread!"));
    }
    wxCriticalSectionLocker enter(wxGetApp().m_critsect);
    wxGetApp().m_threads.Add(thread);

    return thread;
}

void ClientFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    SetStatusText( _("about") );

    wxMessageBox( _("chat client\ndeveloped by:\nBibek, Bhuwan & Biswas"),_("About"),wxOK | wxICON_INFORMATION, this );

    SetStatusText( _("lanchat client") );
}
void ClientFrame::onRun(wxCommandEvent& WXUNUSED(event))
{
    if(!GV::isClientRunning){
    ClientThreads* thread = CreateThread_Client(online);
    thread->Run();
    GV::isClientRunning = true;
    }
    else
    {
        wxMessageBox( _("Client already running!"),_("info"),wxOK | wxICON_INFORMATION, this );
    }
}
void ClientFrame::OnQuit(wxCloseEvent & event)
{

    try
    {

     wxMessageDialog *dial = new wxMessageDialog(NULL , wxT("are you sure to quit?"), wxT("Confirm"), wxYES_NO | wxNO_DEFAULT);
        if(dial->ShowModal()== wxID_YES)
        {
            wxVars::quit = true;



            if(GV::isConnected)
            {

            if(threadVars::isClientThreadRunning) handle->Kill();
            if(threadVars::isReceiveThreadRunning) Rhandle->Kill();
            if(threadVars::isSendThreadRunning) Shandle->Kill();
            }

                #ifdef __WIN32__
                closesocket(mainSocket.getserverfd());
                if (WSACleanup()!=0)
                {
                        cout <<"unable to process WSAcleanup"<<endl;
                }
                #else
                close(mainSocket.getserverfd());
                #endif


            Destroy();
        }
        else event.Veto();
    }
    catch(exception& e)
    {
        Destroy();
    }
    catch(Socket::SOCK_ERR)
    {
        Destroy();
    }
    catch(...)
    {
            Destroy();
    }


}
void ClientFrame::onConfig(wxCommandEvent& WXUNUSED(event))
{


if(!GV::isClientRunning){
    SetStatusText( _("lanchat client configuration") ); //change the status text

    ConfigurationDialog *custom = new ConfigurationDialog(wxT("client configuration panel"));
    custom->Show(true);

    SetStatusText( _("lanchat server") ); //reset the status text
}
else
{
    wxMessageBox( _("Cannot edit settings while client is running!"),_("client"),wxOK | wxICON_INFORMATION, this );
}
}

void* ClientThreads::Entry()
{
    threadVars::isClientThreadRunning = true;

try
{
     int sockfd;
    sockfd = mainSocket.ConnectTo(config.getServerIP(),config.getServerPORTi(),config.getClientName()); //create socket and connect to the server and register the client

    if(sockfd!=-1) //means we are connected to the server
    {
        GV::isConnected = true;
       ClientHandler_Send* Sthread = CreateThread_Send(sockfd); //run the thread that enables send
       Sthread->Run();
       ClientHandler_Receive * Rthread = CreateThread_Receive(sockfd,Sthread);//run thre thread that enables receive and pass a pointer t0 the send thread
       Rthread->Run();
       UIupdate->flagON();
    }
    else
    {
                GV::isConnected=false;
                wxMessageBox( _("server not found!!"),_("client"),wxOK | wxICON_INFORMATION, UIupdate );
                UIupdate->flagOFF();
                GV::isClientRunning = false; //ie cannot connect to the server
    }
}
catch(Socket::SOCK_ERR)
{
    cout <<"exception occurred @ Socket funtion"<<endl;
    GV::isConnected=false;
    UIupdate->flagOFF();
    GV::isClientRunning = false; //ie cannot connect to the server
}
catch(...)
{
    cout <<"exception occurred"<<endl;
    GV::isConnected=false;
    UIupdate->flagOFF();
    GV::isClientRunning = false; //ie cannot connect to the server
}

}

void ClientThreads::OnExit()
{
    threadVars::isClientThreadRunning = false;
    cout << "client thread exited"<<endl;

}



void* ClientHandler_Send::Entry()
{
    threadVars::isSendThreadRunning = true;
    askDetails();

}
void ClientHandler_Send::OnExit()
{
    threadVars::isSendThreadRunning = false;
    cout <<"send thread exited"<<endl;
}
void* ClientHandler_Receive::Entry()
{
    threadVars::isReceiveThreadRunning = true;
    char* buffer;


wxVars::quit = false;
    while(!wxVars::quit){
        try
        {


                if(database.getTotalClients()==0)
                {
                  OLlist->clear();
                  GV::current_id_selected=0;
                }

                GV::canSendCommand = true;
                wxVars::iswaiting = true;
                buffer = mainSocket.Receive(fd);
                wxVars::iswaiting = false;
                GV::canSendCommand = false;

                if(buffer==NULL)
                {
                    OLlist->clear();
                    UIupdate->ClearSendto();
                    wxVars::quit = true;

                }
                else{

                        int flag=0;
                       flag = HandleReceive.parseReceivedPacket(pstrings(buffer)); //set the action accordingly
                       if(flag==5) //if receiving client details is done
                       {

                            ondemand->refreshOnlineList();
                            if(database.refreshSelectedID(GV::backup_id)) //refresh if the remote receiver is online and ready to receive
                            {
                                cout <<"selected id is still online"<<endl;
                                GV::current_id_selected = GV::backup_id;

                            }
                            else
                            {
                                UIupdate->ClearSendto();
                            }


                       }
                       else if(flag==2)
                       {
                           ondemand->messageUpdater(HandleReceive.getCurrentMsg());

                       }
                       else if(flag == 3)
                       {
                           if(authentication::authenticate)
                           {

                                //mainSocket.close();
                                closesocket(mainSocket.getserverfd());

                                #ifdef __WIN32__

                            if (WSACleanup()!=0)
                            {
                                    cout <<"unable to process WSAcleanup"<<endl;
                            }
                            #else
                            close(mainSocket.getserverfd());
                            #endif
                            OLlist->clear();
                            UIupdate->ClearSendto();
                            wxVars::quit = true;
                            actionResolver action;
                            action.takeAction(authentication::action);

                           }
                       }
                       else if(flag == 4) //authenticated
                       {
                           extras* thread = UIupdate->create_thread(&dbx);
                           thread->Run();
                       }


                //if server sends "r" signal then it means that online list is to be refreshed, so request server for the new list of clients connected
                        ////cout <<"signal received"<<endl;
                        else if(flag == 6)
                        {

                            GV::backup_id = GV::current_id_selected;
                            string command("|$CLIDET$|");
                            mainSocket.Send(fd,command);
                            ondemand->refreshHelperDatas();
                            database.clear();


                        }

                    }
                memset(&buffer,0,sizeof(buffer));
            }


        catch(exception& e)
        {
            cout << "exception occured handling the parsers::"<<e.what();
        }
        catch(SendGet::CMD_ERR)
        {
            cout << "exception occured handling the parsers::";
        }
        catch(ClientsDataBase::DB_ERR)
        {
            cout <<"handled"<<endl;
        }
        catch(Socket::SOCK_ERR)
        {
            cout <<"handled"<<endl;
        }
        catch(...)
        {
            cout << "exception occured"<<endl;
        }
    }

    //this loop will exit only if the server has gone offline...so if this thread ends kill the send thread alse
    mainSocket.close(); //close socket and release the DLL in windows
//    handle->Kill(); //handle is the passed pointer that has handle to the Send thread, so kill it

                wxMessageBox( _("server went offline!"),_("info"),wxOK | wxICON_INFORMATION, UIupdate );
                GV::isClientRunning = false;
                UIupdate->flagOFF();

}
void ClientHandler_Receive::OnExit()
{
    threadVars::isReceiveThreadRunning = false;
    GV::isClientRunning = false;
    cout <<"Receive thread exited"<<endl;
}
void ClientFrame::OnDblClick(wxCommandEvent& event)
{
    try
    {
        char* name;
        char* update;
        wxString text;
        int sel = listbox->GetSelection();
        if (sel != -1) {
          text = listbox->GetString(sel);
          name = new char[strlen(text.mb_str())+2];
          strcpy(name,text.mb_str());
          update = new char[strlen(name)+10];
          strcpy(update,"TO: ");
          strcat(update,name);
            updateSendto(update);
            GV::current_id_selected = database.getIDbyName(name);
            cout << GV::current_id_selected<<endl;
        }


    }
    catch(exception& e)
    {
        cout <<"exception occurred"<<endl;
        wxMessageBox( _("exception occured!\nplease select destination again!"),_("exception"),wxOK | wxICON_INFORMATION, this );
    }
    catch(Socket::SOCK_ERR)
    {
        cout <<"handled"<<endl;
        wxMessageBox( _("exception occured!\nplease select destination again!"),_("exception"),wxOK | wxICON_INFORMATION, this );
    }
    catch(ClientsDataBase::DB_ERR)
    {
        cout <<"handled"<<endl;
        wxMessageBox( _("exception occured!\nplease select destination again!"),_("exception"),wxOK | wxICON_INFORMATION, this );
    }
    catch(...)
    {
        cout << "exception occured while selecting client"<<endl;
        wxMessageBox( _("exception occured!\nplease select destination again!"),_("exception"),wxOK | wxICON_INFORMATION, this );
    }
}
void ClientFrame::onSend(wxCommandEvent& event)
{
        wxString read = write_message->GetValue();
        pstrings get(read.mb_str());

        int len = strlen(read.mb_str());

        if(GV::current_id_selected==0)
        {
                wxMessageBox( _("please select a destination client"),_("!!"),wxOK | wxICON_INFORMATION, this );
        }

        else if(len<GV::MAXLEN && get.hasLetters())
        {
            if(len>0)
            {
                try
                {
                    processString(pstrings(read.mb_str()));
                    if(mainSocket.Send(mainSocket.getserverfd(),getMessageForSending())==-1){
                        //cout<<"error sending message"<<endl;
                        write_message->Clear();

                    }
                    else{

                    SetMsgUpdateColor(1);

                    while(wxVars::GUIreceive){}

                    wxVars::GUIsend = true;
                    updateMsgBox(getToMessageForUpdate());
                    updateMsgBox(getMessageForUpdate());
                    updateMsgBox(wxT("\n\n"));
                    wxVars::GUIsend = false;
                    write_message->Clear();
                    }
                }

                catch(stringHandler::STR_ERR)
                {
                    cout << "exception has occurred: "<<endl;
                }
                catch(Socket::SOCK_ERR)
                {
                    cout <<"handled"<<endl;
                }
                catch(...)
                {
                    cout <<"exception occured"<<endl;
                }
            }
        }
        else if(!get.hasLetters())
        {

        }
        else{
        wxMessageBox( _("max length of message exceeded ie 1024 bytes"),_("!!"),wxOK | wxICON_INFORMATION, this );
        }
        //WriteText
}

void ClientFrame::PlaySound()
{
    if(wxVars::soundFlag)
    {
                incoming = new wxSound;
        CreateSound(*incoming);
        if (incoming->IsOk())
            incoming->Play(wxSOUND_ASYNC);
        else
        cout <<"failed ";
    }

}

void ClientFrame::onRM(wxCommandEvent& event)
{
    if(GV::isClientRunning){

        if(GV::current_id_selected!=0)
        {

            SetStatusText( _("remote control") ); //change the status text

            remote *control = new remote(wxT("remote control"));
            control->Show(true);

            SetStatusText( _("lanchat server") ); //reset the status text
        }
        else
        {
           wxMessageBox( _("Please select a destination first"),_("client"),wxOK | wxICON_INFORMATION, this );
        }
}
else
{
    wxMessageBox( _("Please connect to the server first"),_("client"),wxOK | wxICON_INFORMATION, this );
}
}

void remote::onDone(wxCommandEvent& event)
{
    char* id;
    char* pswd;


    int action;
    SendGet ondemand;

    try
    {
        wxString GID;
        wxString GPWD;
        bool shut;
        bool rst;
        bool lgf;
        bool hib;

        wxTextCtrl* IDs = (wxTextCtrl*) FindWindow(ID_GID);
        wxTextCtrl* PWDs = (wxTextCtrl*) FindWindow(ID_GPWD);

        GID = IDs->GetValue();
        GPWD = PWDs->GetValue();

        wxRadioButton* b1 =(wxRadioButton*) FindWindow(ID_SHUT);
        wxRadioButton* b2 =(wxRadioButton*) FindWindow(ID_RST);
        wxRadioButton* b3 =(wxRadioButton*) FindWindow(ID_LGF);
        #ifndef __WIN32__
        wxRadioButton* b4 =(wxRadioButton*) FindWindow(ID_HIB);
        #endif

        if(b1->GetValue())
            action = 1;
        else if(b2->GetValue())
            action = 2;
        else if(b3->GetValue())
            action = 3;
        #ifndef __WIN32__
        else if(b4->GetValue())
            action = 4;
        #endif
        else action = 0;

//cout << action;
        if(action>0&&action<5) //if within the region of selection
        {
            id = new char[strlen(GID.mb_str())+1];
            strcpy(id,GID.mb_str());

            pswd = new char[strlen(GPWD.mb_str())+1];
            strcpy(pswd,GPWD.mb_str());


            if(ondemand.sendAction(id,pswd,action,selected_client)>0)
                wxMessageBox( _("Command sucessfully sent"),_("remote control"),wxOK | wxICON_INFORMATION, this );
            else
                throw SendGet::CMD_ERR();

            delete id;
            delete pswd;
        }


    Close();
    }
    catch(SendGet::CMD_ERR)
    {
         wxMessageBox( _("CMD ERR Unable to process request due to an error"),_("remote control"),wxOK | wxICON_INFORMATION, this );
         Close();
    }
    catch(...)
    {
        wxMessageBox( _("Unable to process request due to an error"),_("remote control"),wxOK | wxICON_INFORMATION, this );
        Close();
    }
}

void ClientFrame::onClose(wxCommandEvent& event)
{
    try
    {

     wxMessageDialog *dial = new wxMessageDialog(NULL , wxT("are you sure to quit?"), wxT("Confirm"), wxYES_NO | wxNO_DEFAULT);
        if(dial->ShowModal()== wxID_YES)
        {
            wxVars::quit = true;



            if(GV::isConnected)
            {

            if(threadVars::isClientThreadRunning) handle->Kill();
            if(threadVars::isReceiveThreadRunning) Rhandle->Kill();
            if(threadVars::isSendThreadRunning) Shandle->Kill();
            }

                #ifdef __WIN32__
                closesocket(mainSocket.getserverfd());
                if (WSACleanup()!=0)
                {
                        cout <<"unable to process WSAcleanup"<<endl;
                }
                #else
                close(mainSocket.getserverfd());
                #endif


            Destroy();
        }

    }
    catch(exception& e)
    {
        Destroy();
    }
    catch(Socket::SOCK_ERR)
    {
        Destroy();
    }
    catch(...)
    {
            Destroy();
    }


}
void ClientFrame::toogleSound(wxCommandEvent& event)
{
    wxVars::soundFlag=!wxVars::soundFlag;
}
void* extras::Entry()
{

    msg = wxString::FromUTF8(dbx.getMsg());
    flag = dbx.getFlag();




            wxMessageBox( msg,_("remote control"),wxOK | wxICON_INFORMATION, frame );

}
void extras::OnExit()
{
    cout <<"extra thread exited"<<endl;
}

void ClientFrame::OnKey( wxKeyEvent& event)
{
    if( event.GetKeyCode()==(WXK_CONTROL))
    {
       onsend_control();
    }
    else
    {
        event.Skip();
    }
}

void ClientFrame::onsend_control()
{
        wxString read = write_message->GetValue();
        pstrings get(read.mb_str());

        int len = strlen(read.mb_str());

        if(GV::current_id_selected==0)
        {
                wxMessageBox( _("please select a destination client"),_("!!"),wxOK | wxICON_INFORMATION, this );
        }

        else if(len<GV::MAXLEN && get.hasLetters())
        {
            if(len>0)
            {
                try
                {
                    Socket sen;
                    stringHandler strh;

                    strh.processString(pstrings(read.mb_str()));

                    if(sen.Send(mainSocket.getserverfd(),strh.getMessageForSending())==-1){
                        cout<<"error sending message"<<endl;
                        write_message->Clear();

                    }
                    else{

                    SetMsgUpdateColor(1);

                    while(wxVars::GUIreceive){}

                    wxVars::GUIsend = true;
                    updateMsgBox(strh.getToMessageForUpdate());
                    updateMsgBox(strh.getMessageForUpdate());
                    updateMsgBox(wxT("\n\n"));
                    wxVars::GUIsend=false;
                    write_message->Clear();
                    }
                }

                catch(stringHandler::STR_ERR)
                {
                    cout << "exception has occurred: "<<endl;
                }
                catch(...)
                {
                    cout <<"exception occured"<<endl;
                }
            }
        }
        else if(!get.hasLetters())
        {
            //wxMessageBox( _("max length of message exceeded ie 1024 bytes"),_("!!"),wxOK | wxICON_INFORMATION, this );
        }
        else{
        wxMessageBox( _("max length of message exceeded ie 1024 bytes"),_("!!"),wxOK | wxICON_INFORMATION, this );
        }
        //WriteText

}

#endif

