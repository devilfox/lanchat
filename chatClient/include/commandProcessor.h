#ifndef COMMANDPROCESSOR_H
#define COMMANDPROCESSOR_H


#ifndef IOSTREAM_H
#define IOSTREAM_H
#include <iostream>
#endif

#ifndef CSTRING_H
#define CSTRING_H
#include <cstring>
#endif

#ifndef STDLIB_H
#define STDLIB_H
#include <stdlib.h>
#endif

#include <conio.h>

#ifndef NAMESPACE_STD
#define NAMESPACE_STD
using namespace std;
#endif


enum task {CLIDET,CLINOS,SELFID,MSGREC,MSGSEN,IMCLNO,AUTHEN,AUSTAT,SIGEND,SIGREF,NONE}; //defines the set of task to be done


class SendGet:public Socket,public pstrings
{
  private:


    task define_task; //enum data to signify the task
    int no_of_clients;
    bool protectBuffer; //packet to make the command used before it gets reaccesssed
                            //should free the buffer once the command is invoked\

    pstrings flag;
    pstrings packet;
    pstrings Sender_id;
    pstrings buffer;
    pstrings current_parsed_client;
    pstrings buffer1;
    pstrings buffer2;

    pstrings buffers;
    pstrings hi;
    pstrings SID;
    pstrings ID;
    pstrings PASS;
    pstrings ACT;
    pstrings msg;






    int sender_ID; //parsed version of Sender_id string
    //-----------------------------------------------------//
    int flags;



    int no_clns;


    int current_parsed_client_no;



    void parse_received_message();
    void parse_received_details();
    void authenticate();
    void authentication_status();
    void clear()
    {
        packet.clear();
        Sender_id.clear();
         buffer.clear();
         current_parsed_client.clear();
         buffer1.clear();
         buffer2.clear();

         buffers.clear();
         hi.clear();
         SID.clear();
         ID.clear();
         PASS.clear();
         ACT.clear();
         msg.clear();

    }
    public:
    class CMD_ERR{};
    SendGet():protectBuffer(false),define_task(NONE)
    {
        cout <<"constructor of sendget invoked"<<endl;
    }
    pstrings getCurrentClient() const
    {
        return current_parsed_client;
    }
    pstrings getCurrentMsg()const
    {
        return buffer;
    }
    int getCurrentClientID() const
    {
        return current_parsed_client_no;
    }
    int parseReceivedPacket(pstrings received);
    int sendAction(pstrings,pstrings,int,int);

    ~SendGet()
    {
        cout <<"destructor of sendget called"<<endl;
    }

};

int SendGet::parseReceivedPacket(pstrings received)
{

    try
    {

            flags=0; // 1 if asked details, 2 if sendmessagee is received,, anything else it is 0
            clear();
            packet = received;

                if(received[0]!='|' || received[received.length()-1]!='|')
                {
                    //cout<<"command syntax incomplete | | missing"<<endl;
                    return -1;
                }

                if(received[1]!='$' || received[8]!='$')
                {
                    //cout<<"invalid command syntax $command$ missing"<<endl;
                    return -1;
                }

        //if syntax and command count is good then
            if(protectBuffer) //check if the buffer is freed
            {
                //cout << "buffer protected, cannot write to buffer!"<<endl;
                return -1;
            }



        protectBuffer = true;
        //-------------------resolve the task------------------------------------------------//

                 if(received.isMatching(2,"CLIDET",7)) define_task = CLIDET;    //requested client details
            else if(received.isMatching(2,"CLINOS",7)) define_task = CLINOS;    //requested client numbers
            else if(received.isMatching(2,"SELFID",7)) define_task = SELFID;    //sending client details to server
            else if(received.isMatching(2,"MSGREC",7)) define_task = MSGREC;    //requested server/client to receive data
            else if(received.isMatching(2,"MSGSEN",7)) define_task = MSGSEN;    //requested server to send data to specified address
            else if(received.isMatching(2,"IMCLNO",7)) define_task = IMCLNO;    //requested requesting clients' number
            else if(received.isMatching(2,"AUTHEN",7)) define_task = AUTHEN;    //authenticate the password and username
            else if(received.isMatching(2,"AUSTAT",7)) define_task = AUSTAT;    //
            else if(received.isMatching(2,"SIGREF",7)) define_task = SIGREF;    //
            else if(received.isMatching(2,"SIGEND",7)) define_task = SIGEND;    //server signals end of the client list


            else //if task is not resolved
            {
                //cout <<"unknown task!"<<endl;
                define_task = NONE; //no defined task to be done
                return false;
            }

        //-----------------------------------------------------------------------------------//
        protectBuffer = false;

            switch(define_task)
            {
                case CLIDET:
                flags=1;
                //cout<<packet<<endl;
                parse_received_details();
                break;

                case CLINOS: //should be a fn that sends back the no of connected clients to the client that requested
                no_of_clients = no_clns;
                break;

                case SELFID:
                break;

                case MSGREC:
                flags=2;
                parse_received_message();
                break;

                case MSGSEN:
                break;

                case IMCLNO:
                break;

                case AUTHEN:
                flags = 3;
                authenticate();
                break;

                case AUSTAT:
                flags = 4;
                authentication_status();
                break;

                case SIGEND:
                flags = 5;
                break;

                case SIGREF:
                flags = 6;
                break;



            }


            return flags;
    }
    catch (exception& e)
    {
        throw e;
    }
    catch(...)
    {
        throw CMD_ERR();
    }
}
void SendGet::parse_received_message()
{
    try
    {



        pstrings message;
        pstrings sender_id;

        message.clear();
        sender_id.clear();

        sender_id = packet.getString(10,packet.find(')')-1);
        int sender = sender_id.Toint();

        message = packet.getString(packet.find('[')+1,packet.find(']')-1);


        if(database.getName(sender)!=NULL && message.length()>0)
        forRecvMsg.processString2(message,database.getName(sender));
        else
        throw ClientsDataBase::DB_ERR();

    }
    catch(exception& e)
    {
        cout<<"handled"<<endl;
    }
    catch(ClientsDataBase::DB_ERR)
    {
        cout<<"handled"<<endl;
    }
    catch(stringHandler::STR_ERR)
    {
        cout<<"handled"<<endl;
    }
    catch(...)
    {
        cout <<"handled"<<endl;
    }
}
void SendGet::parse_received_details()
{
    try
    {
            buffer1 = packet.getString(10,packet.find(']')-1);
            buffer2 = packet.getString(packet.find(']')+2,packet.find(')')-1);
            current_parsed_client_no = buffer2.Toint();
            current_parsed_client = buffer1;

            //cout << current_parsed_client.c_str()<<endl;
            database.addClient(current_parsed_client.c_str(),current_parsed_client_no);
    }
    catch(exception& e)
    {
        cout <<"handled"<<endl;
    }
    catch(ClientsDataBase::DB_ERR)
    {
        cout <<"handled"<<endl;
    }
    catch(...)
    {
        cout <<"handled"<<endl;
    }
}

void SendGet::authenticate()
{
    //cout <<"authenticationg"<<endl;
    //format |$AUTHEN$(ID)[password]<action>|

    try
    {

    int sid_int;
    pstrings msg;
    ACT.clear();
            SID = packet.getString(10,packet.find(')')-1);
            cout << SID<<endl;
            sid_int = SID.Toint();


            ID = packet.getString(packet.find('[')+1,packet.find(']')-1);
            PASS = packet.getString(packet.find('{')+1,packet.find('}')-1);
            ACT = packet.getString(packet.find('<')+1,packet.find('>')-1);



        if(ID == pstrings(config.getRemoteID()) && PASS == pstrings(config.getPassword())  && authentication::checked)
        {
            cout <<"<<authenticated>>"<<endl<<ACT.Toint()<<endl;
            authentication::authenticate = true;
            authentication::action = ACT.Toint();

            msg+"|$AUSTAT$(";
            msg+SID;
            msg+")<1>|";

            mainSocket.Send(mainSocket.getserverfd(),msg.Tostring());

        }

        else
        {
            cout <<"not authenticated"<<endl;
            authentication::authenticate = false;
            authentication::action = 0;

            msg+"|$AUSTAT$(";
            msg+SID;
            msg+")<0>|";

            mainSocket.Send(mainSocket.getserverfd(),msg);
        }
    }
    catch(exception& e)
    {
        cout <<"exception handled"<<endl;
    }
    catch(Socket::SOCK_ERR)
    {
        cout <<"exception handled"<<endl;
    }
    catch(...)
    {
        cout <<"exception handled"<<endl;
    }

}

int SendGet::sendAction(pstrings id, pstrings pswd,int action,int client)
{
    try
    {


        char tion[5],c_cli[5];
        itoa(action,tion,10);
        itoa(client,c_cli,10);

        pstrings curr_client(c_cli); //currently selected client
        pstrings act(tion);




        msg.clear();
        msg+"|$AUTHEN$(";
        msg+curr_client;
        msg+")[";
        msg+id;
        msg+"]{";
        msg+pswd;
        msg+"}<";
        msg+act;
        msg+">|";


        if(Send(mainSocket.getserverfd(),msg)<0)
            throw CMD_ERR();

        delete msg;

    }
    catch(exception& e)
    {
        cout <<"handled"<<endl;
    }
    catch(Socket::SOCK_ERR)
    {
        cout <<"handled"<<endl;
    }
    catch(...)
    {
        cout <<"handled"<<endl;
    }
}

void SendGet::authentication_status()
{
    try
    {

        pstrings res;

        res = packet.getString(packet.find('<')+1,packet.find('>')-1);

            int result = res.Toint();


            if(result == 1) //sucessful
            {

                    dbx.getxtra("command succeeded",1);
            }
            else if (result == 0)
            {

                dbx.getxtra("authentication failed",0);
            }


    }
    catch(exception& e)
    {
        cout << "exception handled--"<<endl;
    }
    catch(...)
    {
        cout << "exception handled--"<<endl;
    }
}

SendGet HandleReceive; //get a object to SendGet

#endif

