#ifndef ADDINS_H
#define ADDINS_H

#ifdef __WIN32__
#include <windows.h>
#pragma comment(lib, "user32.lib")
#pragma comment(lib, "advapi32.lib")
#endif

class actionResolver
{

    public:
    bool takeAction(int);
};
bool actionResolver::takeAction(int action)
{
    #ifdef __WIN32__
        HANDLE hToken;
       TOKEN_PRIVILEGES tkp;

       // Get a token for this process.

       if (!OpenProcessToken(GetCurrentProcess(),
            TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
          return( false );

       // Get the LUID for the shutdown privilege.

       LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,&tkp.Privileges[0].Luid);

       tkp.PrivilegeCount = 1;  // one privilege to set
       tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

       // Get the shutdown privilege for this process.

       AdjustTokenPrivileges(hToken, false, &tkp, 0,
            (PTOKEN_PRIVILEGES)NULL, 0);

       if (GetLastError() != ERROR_SUCCESS)
          return false;

       // Shut down the system and force all applications to close.
    if(action == 1) //shutdown
    {
       if (!ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE,NULL))
          return false;
    }
    else if (action == 2) // Restart
    {
        if (!ExitWindowsEx(EWX_REBOOT | EWX_FORCE,NULL))
          return false;
    }
    else if(action == 3) //Logoff
    {
        if (!ExitWindowsEx(EWX_LOGOFF | EWX_FORCE,NULL))
          return false;
    }



       //shutdown was successful
       return true;

   #else //else code

   #endif

}










#endif


