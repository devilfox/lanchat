//wx_implement.h serves major file in the project that
//has wxWidget implementations
#ifndef WX_IMPLEMENT
#define WX_IMPLEMENT


#ifndef _WX_WX_H_ //avoiding multiple inclusions
#include"wx/wx.h"
#endif

WX_DEFINE_ARRAY_PTR(wxThread *, wxArrayThread);

class Client : public wxApp
{
    public:
    Client();
    virtual bool OnInit();
        // all the threads currently alive - as soon as the thread terminates, it's
    // removed from the array
    wxArrayThread m_threads;

    // crit section protects access to all of the arrays below
    wxCriticalSection m_critsect;

    // semaphore used to wait for the threads to exit, see MyFrame::OnQuit()
    wxSemaphore m_semAllDone;

    // the last exiting thread should post to m_semAllDone if this is true
    // (protected by the same m_critsect)
    bool m_waitingUntilAllDone;


};

Client::Client(): m_semAllDone()
{
        m_waitingUntilAllDone = false;
}

IMPLEMENT_APP(Client) //implementing the Client class


#endif
