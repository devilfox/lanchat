#ifndef STRINGS_H
#define STRINGS_H

#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <time.h>
#include <vector>

using namespace std;

class pstrings
{
private:
    char* temp;
    string buf;

    bool good;
public:

    pstrings();

    pstrings(const string in); //copy constructor

    pstrings(const char* in); //copy constructor

    operator char*();

    operator string();

    void operator = (const string in);

    void operator = (const char* in);

    void operator = (const int in);

    void clear();

    int length();

    void insert(const char ch);

    void insert(const int pos,const int count,const char ch);

    void append(const int count,const char ch);

    int find(const char ch);

    int find(const int flag, const char ch); //flag denotes the position form start from where the search for charcater begins

    int find(const int start,const char ch,const int end); //searches for the charcater within the given region

    bool isMatching(const int start,pstrings in, const int end); //is the string matching within given region?

    bool isMatching(const int start,string in, const int end);

    bool isMatching(const int start,char* in, const int end);

    pstrings getString(const int start, const int end);

    int concurrence(const int end , const char ch );

    void breakpos(const int pos);

    bool isgood();

    void operator + (const pstrings in); //concates the string to the calling object.so has no return value

    void operator + (const string in); //concates the string to the calling object.so has no return value

    void operator + (const char* in);//concates the string to the calling object.so has no return value

    bool operator == (const pstrings in);

    bool operator == (const string in);

    bool operator == (const char* in);

    bool operator != (const pstrings in);

    bool operator != (const string in);

    bool operator != (const char* in);

    char operator [](int index);

    int Toint();

    pstrings Tostring(int in);

    string Tostring();

    char* c_str();

    void toUpper();

    bool hasLetters();  //checks if the string have alphanumeric values

    void encrypt();

    void decrypt();

    friend ostream& operator << (ostream& out,pstrings in);

    ~pstrings();
};

inline pstrings::pstrings()
{
    //cout <<"pstrings constructor"<<endl;
    char* temp = new char[1];
    temp[0]='\0';
}

inline pstrings::pstrings(const string in)
{
    buf.clear();
    buf = in;

    char* temp = new char[1];
    temp[0]='\0';
}

inline pstrings::pstrings(const char* in)
{
    buf.clear();
    buf = in;

    char* temp = new char[1];
    temp[0]='\0';
}

inline pstrings::operator char*()
{
    temp = new char[buf.length()+1];
    strcpy(temp,buf.c_str());
    return temp;
}

inline pstrings::operator string()
{
    return buf;
}
inline void pstrings::clear()
{
    buf.clear();
}

inline void pstrings::operator = (const string in)
{
    buf.clear();
    buf = in;
}

inline void pstrings::operator = (const char* in)
{
    buf.clear();
    buf = in;
}

inline void pstrings::insert(const char ch)
{
    char tmp[2]; //create a string of the character to be inserted
    tmp[0]=ch;
    tmp[1]='\0';

    char* temp = new char[buf.length()+3];
    strcpy(temp,buf.c_str());
    strcat(temp,tmp);

    buf.clear();
    buf = temp;

    delete temp;

}

inline void pstrings::operator + (const pstrings in)
{
    string temp = buf + in.buf;
    buf.clear();
    buf = temp;
}

inline void pstrings::operator + (const string in)
{
    string temp = buf + in;
    buf.clear();
    buf = temp;
}
inline void pstrings::operator + (const char* in)
{
    string temp = buf + string(in);
    buf.clear();
    buf = temp;
}
inline bool pstrings::operator == (const pstrings in)
{
    return (buf == in.buf);
}
inline bool pstrings::operator == (const string in)
{
    return (buf == in);
}
inline bool pstrings::operator == (const char* in)
{
    return(buf == string(in));
}
inline bool pstrings::operator != (const pstrings in)
{
    return (buf != in.buf);
}
inline bool pstrings::operator != (const string in)
{
    return (buf != in);
}
inline bool pstrings::operator != (const char* in)
{
    return (buf != string(in));
}
inline int pstrings::length()
{
    return buf.length();
}
inline int pstrings::find(const char ch)
{
  for(int i = 0; i< buf.length();i++)
  {
      if(buf[i]==ch)    return i;
  }
  return -1;
}
inline int pstrings::find(const int flag,const char ch)
{
    if(flag <= buf.length()-1)
    {
        for(int i=flag;i<buf.length();i++)
        {
            if(buf[i]==ch) return i;
        }
        return -1;
    }
    else return -1;
}

inline int pstrings::find(const int start, const char ch, const int end)
{
    if(start != end && start >= 0 && start <= buf.length()-1 && end >= 0 && end <= buf.length()-1)
    {
        for(int i=start;i<=end;i++)
        {
            if(buf[i]==ch) return i;
        }
        return -1;
    }
    else return -1;
}

inline char pstrings::operator [] (int index)
{
    return buf[index];
}

inline bool pstrings::isMatching(const int start , pstrings in, const int end)
{
    if(start != end && start >= 0 && start <= buf.length()-1 && end >= 0 && end <= buf.length()-1 && end-start == in.length()-1)
    {
        pstrings temp;
        for(int i=start;i<=end;i++)
        {
            temp.insert(buf[i]);
        }

        if(temp==in)    return true;
        else return false;
    }
    else return false;

}
inline bool pstrings::isMatching(const int start , string in, const int end)
{
    if(start != end && start >= 0 && start <= buf.length()-1 && end >= 0 && end <= buf.length()-1 && end-start == in.length()-1)
    {
        pstrings temp;
        for(int i=start;i<=end;i++)
        {
            temp.insert(buf[i]);
        }
        string tmp = temp;
        if(tmp==in)    return true;
        else return false;
    }
    else return false;

}

inline bool pstrings::isMatching(const int start , char* in, const int end)
{
    int len = strlen(in);
    if(start != end && start >= 0 && start <= buf.length()-1 && end >= 0 && end <= buf.length()-1 && end-start == len-1)
    {
        pstrings temp;
        char* tmp;
        for(int i=start;i<=end;i++)
        {
            temp.insert(buf[i]);
        }
        tmp = temp;
        if(strcmp(tmp,in)==0)
        {
            delete tmp;
            return true;
        }
        else
        {
          delete tmp;
          return false;
        }
    }
    else return false;

}

pstrings pstrings::getString(const int start,const int end)
{
    if(start >= 0 && start <= buf.length()-1 && end >= 0 && end <= buf.length()-1)
    {

        pstrings stemp;
        stemp.clear();
        for(int i = start;i<=end;i++)
            stemp.insert(buf[i]);

        good = true;
        return stemp;

    }
    else
    {
        good = false;
        return pstrings("");
    }

}

bool pstrings::isgood()
{
    return good;
}

int pstrings::Toint()
{
    return atoi(buf.c_str());
}
pstrings pstrings::Tostring(int in)
{
    char tmp[5];
    itoa(in,tmp,10);
    buf = tmp;
    return pstrings(buf);
}
void pstrings::operator = (const int in)
{
    char tem[10];
    itoa(in,tem,10);
    buf = tem;
}
char* pstrings::c_str()
{
    char* tmp = new char[buf.length()+1];
    strcpy(tmp,buf.c_str());
    return tmp;
}
string pstrings::Tostring()
{
    return buf;
}

void pstrings::toUpper()
{
    for(int i=0;i<buf.length();i++)
    {
        buf[i] = toupper(buf[i]);
    }
}
int pstrings::concurrence(const int end,const char ch)
{
    int count = 0;
    if(end >= 0 && end <= buf.length()-1)
    {

        for(int i=end;i>=0;i--)
        {
            if(buf[i]==ch) count++;
            else break;
        }

    }
    return count;
}
void pstrings::breakpos(const int pos)
{
    pstrings tmp;

    for(int i=0;i<pos;i++)
        tmp.insert(buf[i]);
    buf.clear();
    buf = tmp.buf;
}
void pstrings::insert(const int pos, const int count,const char ch)
{
   pstrings tmp(buf);
    tmp.breakpos(pos);
    for(int i=0;i<count;i++)
        tmp.insert(ch);

    tmp+getString(pos,buf.length()-1);

    buf.clear();
    buf = tmp.buf;

}
void pstrings::append(const int count,const char ch)
{
    pstrings tmp(buf);
    for(int i=0;i<count;i++)
        tmp.insert(ch);
    buf.clear();
    buf = tmp.buf;
}
bool pstrings::hasLetters()
{
    for(int i=0;i<buf.length();i++)
    {
        if(buf[i]!='\n' && buf[i] != ' ')
            return true;

    }
    return false;
}
void pstrings::encrypt()
{

    pstrings data;
    pstrings temp;

    vector<int>key_space;
    int key;

    srand(time(NULL));
    key = rand();
    temp.Tostring(key);
        key_space.push_back(key);
        for(int i=1;i<buf.length();i++)
        {
            key_space.push_back(key+i*2);
            key++;
        }

        for(int i=0;i<buf.length();i++)
        {
            buf[i]^=key_space[i];
        }

    data.insert('<');
    data + temp;
    data.insert('>');
    data.insert('{');
    data + buf;
    data.insert('}');

    buf.clear();
    buf = data.Tostring();

}

void pstrings::decrypt()
{
    vector<int>key_space;
    pstrings data = getString(find('{')+1,find('}')-1);
    pstrings temp = getString(find('<')+1,find('>')-1);
    int key = temp.Toint();
    buf.clear();
    buf = data.Tostring();

     key_space.push_back(key);
        for(int i=1;i<buf.length();i++)
        {
            key_space.push_back(key+i*2);
            key++;
        }

        for(int i=0;i<buf.length();i++)
        {
            buf[i]^=key_space[i];
        }
}

ostream& operator << (ostream& out , pstrings in)
{
    out << in.buf;
    return out;
}
inline pstrings::~pstrings()
{
    //cout <<"pstrings destructor "<<endl;
}


#endif

