#include "include/pstrings.h"
#include <exception>
#include "include/addins.h"
#include "include/variables.h"
#include "include/core_cpp_implement.h"
#include "include/data_enc.h"
#include "include/files.h"
#include "include/socket_implement.h"
#include "include/ClientDatabaseHandler.h"
#include "include/stringmanipulation.h"
#include "include/commandProcessor.h"
#include "include/wx_implement.h"
#include "include/wxClasses.h"
#include "include/wx_class_implement.h"


void Ehandler()
{
    cout <<"Ehandler received exception"<<endl;
}


//this function of class Client acts as the main function
bool Client::OnInit()
{
    set_terminate(Ehandler); //override terminate() on unhandled exception
    set_unexpected(Ehandler); //set the unexpected exception handler

    try
    {
        config.readFile(); //load configuration file on start
    }
    catch(exception& e)
    {
        cout <<"exception occured"<<endl;
    }
    catch(configuration::FILE_ERR)
    {
        cout <<"exception occurred reading/writing configuration file"<<endl;
    }
    catch(DataSecurity::DAT_ERR)
    {
        cout <<"exception occurred reading/writing configuration file"<<endl;
    }
    catch(...)
    {
        cout << "exception occured"<<endl;
    }

    ClientFrame* ServerWindow = new ClientFrame(wxT(TITLE));

    ServerWindow->Show(true);

    return true;
}
